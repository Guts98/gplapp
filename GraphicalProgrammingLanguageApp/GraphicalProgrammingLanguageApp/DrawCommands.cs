﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// DrawCommand class that inherits <see cref="Command"/> abstract class
    /// It holds all the valid draw command tokens in its token hashtable and
    /// also contains a parse command method 
    /// </summary>
    public class DrawCommands:Command
    {
        /// <summary>
        /// Default constructor that adds all the valid draw command tokens to the base class's tokens
        /// </summary>
        public DrawCommands()
        {
            base.tokens.Add("cmdMOVETO", "moveto");
            base.tokens.Add("cmdDRAWTO", "drawto");
            base.tokens.Add("cmdSQUARE", "square");
            base.tokens.Add("cmdRECT", "rect");
            base.tokens.Add("cmdTRIANGLE", "triangle");
            base.tokens.Add("cmdCIRCLE", "circle");
            base.tokens.Add("cmdSTAR", "star");
            base.tokens.Add("cmdPOLYGON", "polygon");

            base.tokens.Add("cmdPENSIZE", "pensize");
            base.tokens.Add("cmdPENCOLOR", "pencolor");
            base.tokens.Add("cmdFILLCOLOR", "fillcolor");
            base.tokens.Add("cmdFILL", "fill");

            base.tokens.Add("cmdCLEAR", "clear");
            base.tokens.Add("cmdRESET", "reset");
            base.tokens.Add("cmdCENTER", "center");
            base.tokens.Add("cmdRUN", "run");
        }

        /// <summary>
        /// Executes a command
        /// Not implemented yet
        /// </summary>
        public override void Execute()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The parser method that takes one draw command line at a time, and
        /// analyzes them based on the first token of the line and 
        /// </summary>
        /// <param name="objectList">parameter list containing line to be parsed,
        /// canvas object to draw on, shapeslist arraylit to add the created shapes object in, 
        /// and variables hashtable containing variables names and values </param>
        public override void parseCommand(params object[] objectList) 
        {
            String line = (String)objectList[0];            //Line to be parsed
            Canvas outputCanvas = (Canvas)objectList[1];    //Canvas object on which the shapes are to be drawn
            ArrayList shapesList = (ArrayList)objectList[2]; //arraylist to store the created shape objects
            Hashtable variables = (Hashtable)objectList[3];   //stored variable names with their values

            //if the line is empty or meant for commenting
            if (line.Equals("") || line[0].Equals('#'))
                return;

            line = line.ToLower().Trim();                                                   //trimm the trailing whitespaces from the command
            String[] splitLine = line.Split(' ');                                           //split the command into the command name and its parameters
            String command = splitLine[0];                                                  //first part stored as command
            if (splitLine.Length > 2)
            {
                throw new InvalidSyntaxException("Wrong number of tokens '"+ splitLine.Length + "' supplied, 2 expected");                                   //when the overall syntax is wrong
            }
            //If invalid command is typed
            if (!(command.Equals("drawto") || command.Equals("square") || command.Equals("rect") || command.Equals("circle") || command.Equals("moveto") ||
                command.Equals("triangle") || command.Equals("halftriangle") || command.Equals("run") || command.Equals("clear") || command.Equals("reset") ||
                command.Equals("pensize") || command.Equals("pencolor") || command.Equals("fill") || command.Equals("fillcolor") || command.Equals("center")
                || command.Equals("star") || command.Equals("polygon")))
            {

                throw new InvalidCommandException("Invalid command '" + command + "' encountered");                                  //when command name is invalid
            }

            //if there exists the parameters along with the command
            else if (splitLine.Length > 1)
            {
                String[] parameters = splitLine[1].Split(',');                              //second part stored in array to be further split by comma
                int[] parametersInt = new int[parameters.Length];                           //int array for storing parameters

                //if parameters are not supposed to take string parameters but these will
                if (!(command.Equals("pencolor") || command.Equals("fill") || command.Equals("fillcolor")))
                {
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        try
                        {
                            parametersInt[i] = Convert.ToInt32(parameters[i].Trim());            //convert string parameters to int 
                        }
                        catch
                        {   //when the format of the parameter is wrong
                            throw new InvalidParameterException("Wrong parameter format '"+ parameters[i].Trim() + "' given"); 
                        }
                    }
                }
                if (command.Equals("rect") || command.Equals("circle") || command.Equals("square") || command.Equals("triangle") || command.Equals("drawto") 
                    || command.Equals("star") || command.Equals("polygon"))
                {
                    ShapeFactory factory = new ShapeFactory();
                    //This block of codes basically makes a new integer array with a size 2 greater than the number of parameters provided by the user
                    //i.e. parametersInt, not first 2 indixes of this new array is assigned the current position of the cursor on the canvas 
                    //lastly in the for loop the values stored in the parametersInt are stored in the new array from the 3rd index
                    int[] shapeParams = new int[parametersInt.Length + 2];
                    shapeParams[0] = outputCanvas.XPos;
                    shapeParams[1] = outputCanvas.YPos;
                    int spIndex = 2;
                    for (int i = 0; i < parametersInt.Length; i++)
                    {
                        shapeParams[spIndex] = parametersInt[i];
                        spIndex++;
                    }

                    Shape shape = factory.getShape(command);                //new shape object as returned by shape factory shape
                    shape.set(outputCanvas.pen.Color, shapeParams);         //sets the pen color and parameter values of the shape
                    //sets the additional properties of the shape
                    shape.FillStatus = outputCanvas.fillStatus;
                    shape.FillColor = outputCanvas.fillColor;
                    shape.PenSize = outputCanvas.pen.Width;
                    shape.PenColor = outputCanvas.pen.Color;
                    shapesList.Add(shape);

                    //update the cursor  points if a line is drawn
                    if (command.Equals("drawto"))
                    {
                        outputCanvas.XPos = shapeParams[2];
                        outputCanvas.YPos = shapeParams[3];
                    }
                }
                else
                {
                    //Switch cases to call associated methods for different commands with parameters
                    switch (command)
                    {
                        case "pensize":                            //sets the width of the pen
                            outputCanvas.setPenSize(parametersInt);
                            break;
                        case "pencolor":                          //set the color of the pen
                            outputCanvas.setPenColor(parameters);
                            break;
                        case "fill":                                //set the color of the pen
                            outputCanvas.setFillStatus(parameters);
                            break;
                        case "fillcolor":                          //set the color of the pen
                            outputCanvas.setFillColor(parameters);
                            break;
                        case "moveto":                            //move cursor position on output canvas
                            outputCanvas.moveTo(parametersInt);
                            break;
                        case "clear":                            //if clear command is typed but with parameters                                
                            throw new InvalidParameterException("Wrong number of parameter for  '" + command + "', 0 expected");        //throw new wrong parameter number exception
                        case "run":                               //if clear command is typed but with parameters                                   
                            throw new InvalidParameterException("Wrong number of parameter for  '" + command + "', 0 expected");      //throw new wrong parameter number exception
                        case "reset":                              //if reset command is typed but with parameters                                   
                            throw new InvalidParameterException("Wrong number of parameter for  '" + command + "', 0 expected");      //throw new wrong parameter number exception
                        case "center":                          //if center command is typed but with parameters                                   
                            throw new InvalidParameterException("Wrong number of parameter for  '" + command + "', 0 expected");     //throw new wrong parameter number exception
                        default:                                            //default
                            throw new InvalidCommandException("Invalid command '"+command+"' encountered");                              //when command name is invalid
                    }
                }
            }
            else                            //for single word commands
            {
                switch (command)                                                    //Switch cases to call associated methods for different commands without parameters
                {
                    case "center":                                                               //moves the cursor to the center of the screen                                 
                        outputCanvas.moveTo(new int[] { 482, 227 });
                        break;
                    case "clear":                                                               //clears the screen of output canvas                                  
                        outputCanvas.clear();
                        break;
                    case "run":                                                               //runs the program written on multi line command box                                  
                        GPLForm.multiCommandRun = true;
                        break;
                    case "reset":                                                               //resets the pen and brush                                
                        outputCanvas.reset();
                        break;
                    default:                                                                    //set errorfound to true
                        throw new InvalidCommandException("Invalid command '" + command + "' encountered");                              //when command name is invalid
                }
            }

        }
    }
}
