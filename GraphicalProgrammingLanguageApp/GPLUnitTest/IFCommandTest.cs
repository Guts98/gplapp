﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphicalProgrammingLanguageApp;
using System.Collections;

namespace GPLUnitTest
{
    /// <summary>
    /// Tests the parseCommand method by passing it different valid if commands with both true
    /// and false conditions and checks whether it does adds the statements to the queue properly
    /// </summary>
    [TestClass]
    public class IFCommandTest
    {
        /// <summary>
        /// Tests if the parseCommand method successfully reads the valid If command and its condition, when its true, it should add the 
        /// block statements inside the if to the program queue
        /// </summary>
        [TestMethod]
        public void Test_commandIfTrueValid()
        {
            // ARRANGE
            CommandParser cmdParser = new CommandParser();      //Command Parser object to access its methods
            ProgramQueue pQueue = new ProgramQueue();           //ProgramQueue object to store the lines verified by commandParser method
            Hashtable variables = new Hashtable();              //Stores the variables
            ArrayList lines = new ArrayList();              //Stores the block of lines to be tester
            lines.Add("if 5>2");
            lines.Add("drawto 100,100");
            lines.Add("endif");

            // ACT
            int lineNo = 1;
            foreach(String line in lines)
            {
                cmdParser.parseCommand(pQueue, line, variables, lineNo++);  //calls the command parser method that analyzes the if command

            }
                
            // ASSERT                                           //drawto 100,100 line will be added since if condition was true
            String expectedAddedCommand = "drawto 100,100";
            String actualAddedCommand = pQueue.peek().ToString();
            int expectedQueueSize = 1;
            int actualQueueSize = pQueue.size();
            Assert.AreEqual(expectedAddedCommand, actualAddedCommand,  "If command was not parsed successfully by parseCommand method.");
            Assert.AreEqual(expectedQueueSize, actualQueueSize, 0.01, "If command was not parsed successfully by parseCommand method.");
        }

        /// <summary>
        /// Tests if the parseCommand method successfully reads the valid If command and its condition, when its false, it should not add the 
        /// block statements inside the if to the program queue
        /// </summary>
        [TestMethod]
        public void Test_commandIfFalseValid()
        {
            // ARRANGE
            CommandParser cmdParser = new CommandParser();      //Command Parser object to access its methods
            ProgramQueue pQueue = new ProgramQueue();           //ProgramQueue object to store the lines verified by commandParser method
            Hashtable variables = new Hashtable();              //Stores the variables
            ArrayList lines = new ArrayList();              //Stores the block of lines to be tester
            lines.Add("if 5<2");
            lines.Add("drawto 100,100");
            lines.Add("endif");

            // ACT
            int lineNo = 1;
            foreach (String line in lines)
            {
                cmdParser.parseCommand(pQueue, line, variables, lineNo++);  //calls the command parser method that analyzes the if command
            }

            // ASSERT                   //NO Lines will be added since if condition was false
            int expectedQueueSize = 0;
            int actualQueueSize = pQueue.size();
            Assert.AreEqual(expectedQueueSize, actualQueueSize, 0.01, "If command was not parsed successfully by parseCommand method.");

        }

    }
}
