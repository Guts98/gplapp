﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Iterator interface that declares the methods to be implemented by child classes to
    /// obtain the functionality of an iterator
    /// </summary>
    public interface Iterator
    {
        /// <summary>
        /// Returns the next object from iterator
        /// </summary>
        /// <returns> the next object from iterator</returns>
        Object next();

        /// <summary>
        /// Sets the condition for the iterator
        /// </summary>
        /// <param name="condition">condition for the iterator</param>
        void setWhileCondition(String condition);

        /// <summary>
        /// Returns the condition of the iterator
        /// </summary>
        /// <returns>condition for the iterator</returns>
        String getWhileCondition();

        /// <summary>
        /// Adds statement to the iterator which it later iterates upon
        /// </summary>
        /// <param name="statement">statement to be added</param>
        void addStatement(String statement);

        /// <summary>
        /// Sets the index for the iteration
        /// </summary>
        /// <param name="i">index for the iteration</param>
        void setIndex(int i);

        /// <summary>
        /// Returns the index of the iteration
        /// </summary>
        /// <returns>index of the iteration</returns>
        int getIndex();

        /// <summary>
        /// Returns bool value, true if the iterator has another statement, false otherwise
        /// </summary>
        /// <returns>bool value for iteration</returns>
        bool hasNext();

        /// <summary>
        /// Set true if the loop is the outermost loop of the iteration
        /// </summary>
        /// <param name="result">bool value for continuation of iteration</param>
        void setIsOutermostLoop(bool result);

        /// <summary>
        /// Returns true if the loop is the outermost loop of the iteration
        /// </summary>
        /// <returns>bool value for continuation of iteration</returns>
        bool getIsOutermostLoop();

        /// <summary>
        /// Stores the initial conditional result for the first iteration of the loop
        /// </summary>
        /// <param name="result">initial conditional result</param>
        void setStartingConditionResult(bool result);

        /// <summary>
        ///  Returns the initial conditional result for the first iteration of the loop
        /// </summary>
        /// <returns>initial conditional result</returns>
        bool getStartingConditionResult();

        /// <summary>
        /// Sets the starting line number of the loop block's starting line
        /// </summary>
        /// <param name="num">line number of the loop block's starting line</param>
        void setStartingLineNum(int num);

        /// <summary>
        /// Returns the starting line number of the loop block's starting line
        /// </summary>
        /// <returns>line number of the loop block's starting line</returns>
        int getStartingLineNum();

    }
}
