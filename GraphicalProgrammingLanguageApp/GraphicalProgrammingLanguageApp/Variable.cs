﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Variable Class that definines Variables with generic data type 
    /// NOT IMPLEMENTED / USED
    /// </summary>
    public class Variable : Command
    {
        /// <summary>
        /// Value that is stored in the variable
        /// </summary>
        private int value;

        /// <summary>
        /// Default constructor of variable class
        /// </summary>
        public Variable()
        {
            base.tokens.Add("cmdVAR", "^[$][a-zA-Z_][a-zA-Z0-9_]*$");
        }

        /// <summary>
        /// Initializes the variable's value as provided in parameter
        /// </summary>
        /// <param name="value">Provided value to be stored in variable's value</param>
        public void setValue(int value)
        {
            this.value = value;
        }


        /// <summary>
        /// Set and get value of variable
        /// </summary>
        public int Value { get => value; set => this.value = value; }

        /// <summary>
        /// Executes the sommand stored in variable
        /// </summary>
        public override void Execute()
        {
            //TODO, not yet implemented
            //Does nothing
        }

        public override void parseCommand(params object[] objectList)
        {
            String line = (String)objectList[0];
            String[] splitLine = line.Split(' ');

            Hashtable variables = (Hashtable)objectList[1];

            //Regular expression for checking a valid variable name
            Regex regex = new Regex((String)base.tokens["cmdVAR"], RegexOptions.Compiled);
        }
        //throw new NotImplementedException();
    }
}

