﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Parses the complex cammands like variables, conditionals, loops, etc
    /// </summary>
    public class CommandParser
    {
        /// <summary>
        /// Datatable reference for using its non-static method compute()
        /// </summary>
        DataTable dt;

        /// <summary>
        /// Keeps the track if an 'if' or 'nested if is running' and handle the next block of statements accordingly
        /// </summary>
        Stack ifLookAhead;

        /// <summary>
        ///  Keeps the track if an 'while' or 'nested if is running' and handle the next block of statements accordingly
        /// </summary>
        // Stack<ArrayList> whileLookAhead;
        Stack whileLookAhead;

        /// <summary>
        /// Instance of while repository class that contains a method that returns a new whileiterator object everytime called
        /// </summary>
        WhileRepository WhileRepository;

        /// <summary>
        /// Queue data storage object to store the objects of all the whileiterator created
        /// </summary>
        Queue storedWhileBlocks;

        /// <summary>
        /// Stores the while blocks in this temporary stack just before unloading it into a queue
        /// </summary>
        Stack tempWhileStack;

        Iterator currentWhileBlock, nextWhileBlock = null;
        /// <summary>
        /// Bool value that keeps track if a block of method is being parsed
        /// </summary>
        bool methodLookAhead;

        /// <summary>
        /// Dynamic reference that points to a new method object every time a new method body is being declared
        /// </summary>
        Method myNewMethod;

        /// <summary>
        /// Marker string passed to a while block when a nested one is encountered
        /// </summary>
        const String MARKER_FOR_NEW_WHILE = "__MARKER_FOR_NEW_WHILE__";

        /// <summary>
        /// Stores the method name and a reference top its object
        /// </summary>
        Hashtable storedMethods;

        /// <summary>
        /// Sets and Returns the Stored method name and a reference top its object
        /// </summary>
        public Hashtable StoredMethods { get => storedMethods; set => storedMethods = value; }

        /// <summary>
        /// Default Constructor that initializes the default instance variables
        /// </summary>
        public CommandParser()
        {
            methodLookAhead = false;
            StoredMethods = new Hashtable();
            tempWhileStack = new Stack();
            storedWhileBlocks = new Queue();
            whileLookAhead = new Stack();
            ifLookAhead = new Stack();
            WhileRepository = new WhileRepository();
            dt = new DataTable();                       //new datatable object to use its compute method
        }

        /// <summary>
        /// Breaks down a line, parses and analyzes it based on the very first token of the line
        /// to retrieve the valid executable commands or thow an exception in case found any
        /// </summary>
        /// <param name="programQueue">queue object to add the draw commands on</param>
        /// <param name="line">line to be parsed</param>
        /// <param name="variables">variables object containing all existing variables</param>
        /// <param name="lineNo">lnie number of the line</param>
        public void parseCommand(ProgramQueue programQueue, String line, Hashtable variables, int lineNo)
        {
            //Looks if a method body is being defined
            if (methodLookAhead && !line.Trim().ToLower().Equals("endmethod"))
            {
                Console.WriteLine("Reachead method adding block");
                if (line.Split(' ')[0].Trim().ToLower().Equals("method"))              //check for invalid nested method declarations
                {
                    //throw error
                    Console.WriteLine("Error: Methods cannot be defined inside another method!");
                    throw new MethodDeclarationException("Methods cannot be defined inside another method");
                }
                this.myNewMethod.addStatement(line);                                     //add the line to the method body/object
                return;
            }
            //Looks if a while body is being defined
            if (this.whileLookAhead.Count > 0)             //if a while loop is running
            {
                if (!line.Trim().ToLower().Equals("endwhile") && !(line.Split(' ')[0]).Trim().ToLower().Equals("while"))
                {
                    //gets the WhileIterator object from the top of stack
                    Iterator whileBlock = (Iterator)this.whileLookAhead.Peek();
                    whileBlock.addStatement(line);      //adds the line to the iterator object
                    return;
                }
            }
            line = line.Trim().ToLower();               //remove trailing whitespaces and change to lower
            if (line.Equals("") || line[0].Equals('#')) //if line is empty or it contains '#' at beggining do nothing and return to caller
                return;

            Match errorSyntax = Regex.Match(line, @"^[a-zA-Z_]"); //ensures that the line doesn't start with anything except a alpha or underscore characters
            if (!errorSyntax.Success)                           //throw invalid command error at the beggining of line error
            {
                Console.WriteLine("Error..invalid command at the start of line1!");
                throw new InvalidCommandException("Invalid command '" + (line.Split(' ')[0]) + "' encountered");
            }
            line = filterConditionalOperators(line);       //filters the conditional operators from the line like..
            //Basic Syntax checking done

            String[] splitLine = line.Split(' ');               //split and store the tokens separated by a whitespace
            String firstToken = splitLine[0].Trim();               //remove trailing spaces from the first line
            Command commands;

            /*********************************************************************/
            /*************** CONTROL COMMANDS AND VARIABLES SECTION **************/
            /*********************************************************************/
            commands = new ControlCommands();           //contains the tokens for all the valid word commands
            if (commands.contains(firstToken))         //if the first word is indeed a valid control command
            {
                if (firstToken.Equals(commands.tokens["cmdIF"]))
                {
                    //the below statement firstly, converts the rest of the line after if command back to string from array, then puts the values of the
                    //existing variables in the expressions, and finally computes the expression to get a bool value
                    bool ifConditionResult = computeCondition(putVariableValues(splitArrayToString(splitLine, 1), variables));
                    if (ifConditionResult)       //if 'if' condition is true
                    {
                        Console.WriteLine("If passed!");
                        if (this.ifLookAhead.Count > 0)                         //if there's already an 'if' condition being applied to the statement
                        {
                            if (!(bool)this.ifLookAhead.Peek())                 //check if the previoous 'if' was true or not, if false, this will also be same
                                this.ifLookAhead.Push(false);
                            else
                                this.ifLookAhead.Push(true);
                        }
                        else                                                //else this it is the first/parent 'if' condition being applied which might have nested ifs
                            this.ifLookAhead.Push(false);
                        return;
                    }
                    else                        //if 'if' condition is false
                    {
                        Console.WriteLine("If failed! IF CONDITION: " + splitArrayToString(splitLine, 1));
                        this.ifLookAhead.Push(true);
                        return;
                    }
                }
                else if (firstToken.Equals(commands.tokens["cmdENDIF"]) && line.Equals(firstToken.Trim()))
                {
                    if (this.ifLookAhead.Count > 0)               //if endif appears only after an if statement otherwise throw error
                        this.ifLookAhead.Pop();
                    else
                    {
                        Console.WriteLine("endif requires an if statment before it.");  //throw error
                        throw new InvalidSyntaxException("'endif' requires an 'if' statement before it");
                    }
                    return;
                }
                else if (firstToken.Equals(commands.tokens["cmdWHILE"]))
                {
                    //the below two statement firstly, converts the rest of the line after while command back to string from array, then puts the values of the
                    //existing variables in the expressions, and finally computes the expression to get a bool value
                    String whileCondition = splitArrayToString(splitLine, 1);
                    bool whileConditionResult = computeCondition(putVariableValues(whileCondition, variables));
                    Iterator newWhileBlock = WhileRepository.getIterator();        //new whileiterator object from while repository

                    newWhileBlock.setWhileCondition(whileCondition);               //set the conditional statement for the while iterator
                    newWhileBlock.setStartingConditionResult(whileConditionResult);     //set initial condition result for evaluation
                    newWhileBlock.setStartingLineNum(lineNo);                           //set the line number of the while statement

                    if (this.whileLookAhead.Count == 0)                          //if its the first while loop of a potential nested loop
                    {
                        newWhileBlock.setIsOutermostLoop(true);                 //since this loop will be the outermost loop
                        this.whileLookAhead.Push(newWhileBlock);                //push the new while iterator object to the stact
                    }
                    else
                    {
                        Iterator currentWhileBlock = (Iterator)this.whileLookAhead.Peek(); //get the current while block object at the top of stack
                        currentWhileBlock.addStatement(MARKER_FOR_NEW_WHILE);       //add the while statement to the current while object
                        newWhileBlock.setIsOutermostLoop(false);                 //since this loop will be the inside another loop
                        this.whileLookAhead.Push(newWhileBlock);                //push the new while iterator object to the stact
                    }
                    return;

                }
                else if (firstToken.Equals(commands.tokens["cmdENDWHILE"]) && line.Equals(firstToken.Trim()))
                {
                    if (this.whileLookAhead.Count > 0)                           //if there are while blocks stored
                    {
                        Console.WriteLine("REACHED ENDMETHOD COMMAND");
                        //if the endwhile command is called, the current object of whileiterator stored in the whileLookAhead stack will be popped
                        //out and pushed to the tempWhileStack with the most outer while object being at the top of the stack
                        this.tempWhileStack.Push(this.whileLookAhead.Pop());        //Pop out the while object container into the new temp stack
                        if (this.whileLookAhead.Count == 0 && this.tempWhileStack.Count > 0)
                        {
                            //Since while executing the while block or blocks, the outer most while loop will be needed at the top and that too  
                            //sequentially, so while objects will be transferred to a new queue object to get it sequentially while execution
                            int noOfLoops = this.tempWhileStack.Count;      //total number of while loops stored in the stack
                            //now here, while objects are stored from outer most to the innermost order in the top to bottom of stack
                            //and the same order will be enqued to the queue
                            for (int i = 0; i < noOfLoops; i++)
                            {
                                Console.WriteLine(this.tempWhileStack.Peek().ToString());
                                this.storedWhileBlocks.Enqueue(this.tempWhileStack.Pop());
                            }
                            //now the storedWHileBlocks queue has all the method objects sequentially ordered with the outermost being at the
                            //top of the queue
                            //Iterator currentWhileBlock, nextWhileBlock =null;
                            currentWhileBlock = (Iterator)this.storedWhileBlocks.Peek();   //Gives reference to the while object stored in queue
                            this.storedWhileBlocks.Enqueue(this.storedWhileBlocks.Dequeue()); //Pushes the current while object to the end of queue
                            if (this.storedWhileBlocks.Count > 1)                   //If there are more than one while objects in the queue
                            {
                                nextWhileBlock = (Iterator)this.storedWhileBlocks.Peek();  //now it stores the while object at the top of queue
                            }

                            /*****While Execution Block********/
                            bool outerLoopInitialCondition = currentWhileBlock.getStartingConditionResult();
                            int infiniteLoopBreaker = 1;            //alternatively halt loop if number of loop exceeds 20000

                            while (true && outerLoopInitialCondition && infiniteLoopBreaker++ < 20000)
                            {
                                int index = 1;
                                while (currentWhileBlock.hasNext())
                                {
                                    String statement = currentWhileBlock.next().ToString();
                                    int lineNum = (index++) + currentWhileBlock.getStartingLineNum();
                                    if (statement.Equals(MARKER_FOR_NEW_WHILE))             //Check if there exists a nested loop inside this one
                                    {
                                        //Compute the condition result for the next while block (nested or adjacent)
                                        if (!computeCondition(putVariableValues(nextWhileBlock.getWhileCondition(), variables)))
                                        {
                                            storedWhileBlocks.Enqueue(storedWhileBlocks.Dequeue()); //Pushes the current while object to the end of queue
                                            nextWhileBlock = (Iterator)storedWhileBlocks.Peek();  //now it stores the while object at the top of queue
                                            continue;   //don't parse this command
                                        }
                                        else
                                        {
                                            currentWhileBlock = (Iterator)storedWhileBlocks.Peek();   //Gives reference to the while object stored in queue
                                            storedWhileBlocks.Enqueue(storedWhileBlocks.Dequeue()); //Pushes the current while object to the end of queue
                                            nextWhileBlock = (Iterator)storedWhileBlocks.Peek();  //now it stores the while object at the top of queue
                                            continue;   //don't parse this command
                                        }
                                    }
                                    //recursively call this method to parse the commands inside while block
                                    parseCommand(programQueue, statement, variables, lineNum);
                                }

                                //if the while condition becomes false and its the outermostloop, then break out of the infinite loop
                                if (!computeCondition(putVariableValues(currentWhileBlock.getWhileCondition(), variables)) && currentWhileBlock.getIsOutermostLoop())
                                    break;
                                //else if the while condition is false and its not outer loop
                                else if (!computeCondition(putVariableValues(currentWhileBlock.getWhileCondition(), variables)) && !currentWhileBlock.getIsOutermostLoop())
                                {
                                    currentWhileBlock = (Iterator)storedWhileBlocks.Peek();   //Gives reference to the while object stored in queue
                                    storedWhileBlocks.Enqueue(storedWhileBlocks.Dequeue()); //Pushes the current while object to the end of queue
                                    nextWhileBlock = (Iterator)storedWhileBlocks.Peek();  //now it stores the while object at the top of queue
                                }

                            }
                            storedWhileBlocks.Clear();
                        }

                    }
                    Console.WriteLine("Variable at the end of while block:");
                    foreach (String key in variables.Keys)
                    {
                        Console.WriteLine("VarName: " + key + " Value:" + variables[key]);
                    }
                    return;
                }
                else if (firstToken.Equals(commands.tokens["cmdMETHOD"]))
                {
                    Console.WriteLine("rechead method declaration");
                    if ((this.ifLookAhead.Count != 0) || (this.whileLookAhead.Count != 0))
                    {
                        //throw error method declaration can't be inside a fi or while block
                        Console.WriteLine("error method declaration can't be inside a if or while block");
                        throw new MethodDeclarationException("Method declaration can't be inside a 'if' or 'while' block");
                    }
                    this.methodLookAhead = true;
                    this.myNewMethod = new Method();                 //create new method object
                    this.myNewMethod.MethodSignature = line;         //set method declaration signature
                    this.myNewMethod.MethodSignatureLineNum = lineNo;
                    this.myNewMethod.filterSignature(StoredMethods);
                    return;
                }
                else if (firstToken.Equals(commands.tokens["cmdENDMETHOD"]) && line.Equals(firstToken.Trim()))
                {
                    if ((this.ifLookAhead.Count != 0) || (this.whileLookAhead.Count != 0))
                    {
                        //throw error method declaration can't be inside a fi or while block
                        Console.WriteLine("error method declaration can't be inside a if or while block");
                        throw new MethodDeclarationException("Method declaration can't be inside a 'if' or 'while' block");
                    }
                    Console.WriteLine("rechead endmethod declaration\n" + this.myNewMethod.toString());

                    this.methodLookAhead = false;
                    this.StoredMethods.Add(this.myNewMethod.MethodName, this.myNewMethod);
                    return;
                }
            }
            //else it must be a variable being assigned or reassigned which requires the second token to be an assignment '=' operator
            else if (splitLine.Length > 1 && splitLine[1].Trim().Equals("="))
            {
                if (variables.ContainsKey(firstToken))   //if variable already exists 
                {   //reassign the variable a new value
                    //the below statement firstly, converts the rest of the line after if command back to string from array, then puts the values of the
                    //existing variables in the expressions, and finally computes the expression to get a string value converted containing an integer result
                    String valueStr = computeExpression(putVariableValues(splitArrayToString(splitLine, 2), variables));

                    variables[firstToken] = valueStr;
                    return;
                }
                else                                    //else new variable is initialized and stored
                {   //Assign a new variable
                    //Checks if the new variable name is valid
                    if (!validateTokenName(firstToken.Trim()))
                        return;

                    int num;
                    String valueStr = splitArrayToString(splitLine, 2);
                    valueStr = putVariableValues(valueStr, variables);                //check line and replace with values if existing variables found
                    if (Int32.TryParse(computeExpression(valueStr), out num))              //Check if valid integer is being assigned to the variable
                    {
                        variables.Add(firstToken, computeExpression(valueStr));
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Parameter format exception while assigning variable"
                            + "firstToken: " + firstToken + " valueStr:" + valueStr + ", noOfVars:" + variables.Count);
                        throw new InvalidParameterException("Format exception while computing '" + valueStr + "'");
                    }
                }
            }
            else if (this.StoredMethods.ContainsKey(firstToken))             //if a method is being called
            {
                Method calledMethod = (Method)this.StoredMethods[firstToken];
                calledMethod.filterMethodExecutionSignature(line, variables);
                int i = 1;
                int statementLineNo = calledMethod.MethodSignatureLineNum;
                //Method call execution
                foreach (String methodLine in calledMethod.ExecutableStatements)
                {
                    //recursively call this parser method to execute the statements of the method
                    parseCommand(programQueue, methodLine, variables, (statementLineNo + (i++)));
                }
                return;
            }

            /*********************************************************************/
            /*********************** DRAW COMMANDS SECTION ***********************/
            /*********************************************************************/
            commands = new DrawCommands();                  //Contains the tokens for all the valid draw commands
            if (commands.contains(firstToken))
            {
                if (this.ifLookAhead.Count > 0)                           //if an if condition was encountered before coming to this line
                {
                    if (!(bool)this.ifLookAhead.Peek())     //if if condition was true add statements to heap queue untill endif is met
                    {
                        line = putVariableValues(line, variables);            //Check line and replace with values if existing variables found
                        programQueue.enqueue(line);                         //Add the line directly to the queue that will be executed
                        programQueue.addLineNum(lineNo);                         //Add the line number of this draw command to access it later
                        return;
                    }
                    else                                            //else if if condition was false don't add statements to heap queue untill endif is met
                        return;
                }
                else                                                //else normal flow of program without control commands being encountered
                {
                    line = putVariableValues(line, variables);            //Check line and replace with values if existing variables found
                    programQueue.enqueue(line);                    //Add the line directly to the queue that will be executed
                    programQueue.addLineNum(lineNo);                         //Add the line number of this draw command to access it later
                    return;
                }
            }
            //If reached here means the command was wrong
            throw new InvalidCommandException("Invalid command '" + firstToken + "' encountered");
        }


        /// <summary>
        /// Static method to validate if a given token is a valid candidate for a variable, parameter 
        /// or method name
        /// </summary>
        /// <param name="tokenName">the token to be validated</param>
        /// <returns></returns>
        public static bool validateTokenName(String tokenName)
        {
            Match varNameValidation = Regex.Match(tokenName, @"\b^[a-zA-Z_]+[0-9]*\b");   //Matches for a word starting with _ or alpha characters
            if (!varNameValidation.Success)
            {   //throw variable naming rule violation
                Console.WriteLine("Variable/Method Naming Rule Violation");
                throw new InvalidTokenException(tokenName);
            }
            return true;
        }

        /// <summary>
        /// Takes and array of string and converts it back to string from the index specified to the end of the array
        /// </summary>
        /// <param name="splitLine">string array to be converted to string</param>
        /// <param name="fromIndex">index number of array </param>
        /// <returns>The string converted from array</returns>
        public static String splitArrayToString(String[] splitLine, int fromIndex)
        {
            String line = "";
            for (int i = fromIndex; i < splitLine.Length; i++)
                line += splitLine[i];
            return line;
        }

        /// <summary>
        /// Computes and returns boolean for a conditional expression provided
        /// </summary>
        /// <param name="exp">conditional expression to be computed</param>
        /// <returns></returns>
        public bool computeCondition(String exp)
        {
            DataTable dt = new DataTable();
            try
            {
                //and or
                exp = Regex.Replace(exp, @"\bAND\b", " AND ");       //CONDITIONAL OPERATORS AND
                exp = Regex.Replace(exp, @"\bOR\b", " OR ");       //CONDITIONAL OPERATORS OR

                var result = dt.Compute(exp, "");
                bool boolResult = Convert.ToBoolean(result);
                return boolResult;
            }
            catch (InvalidCastException)
            {
                //throw condition must return bool value error
                Console.WriteLine("Error in computing condition");
                throw new ComputationalException("Error in computing '" + exp + "' condition");
            }
            catch (Exception)
            {
                //throw error in computing condition
                Console.WriteLine("error in computing condition! Exp: " + exp);
                throw new ComputationalException("Error in computing '" + exp + "' condition");
            }
        }

        /// <summary>
        /// Computes and returns string value of integer computed from the exression provided
        /// </summary>
        /// <param name="exp">expression to be computed</param>
        /// <returns></returns>
        public String computeExpression(String exp)
        {
            DataTable dt = new DataTable();
            try
            {
                var result = dt.Compute(exp, "");
                int intResult = Convert.ToInt32(result);
                return intResult.ToString();
            }
            catch (InvalidCastException)
            {
                //throw condition must return integer value error
                Console.WriteLine("expression must return an integer value error, exp:" + exp);
                throw new ComputationalException("Error in computing '" + exp + "' expression");
            }
            catch (Exception)
            {
                //throw error in computing condition
                Console.WriteLine("error in computing expression!");
                throw new ComputationalException("Error in computing '" + exp + "' expression");

            }
        }

        /// <summary>
        /// Filters and returns the conditional operators from the line like 
        /// from '==' to '=', '!=' to '<>', '&&' to 'AND' and '||' to 'OR'.
        /// </summary>
        /// <param name="line">line to be filtered</param>
        /// <returns>filtered line</returns>
        public String filterConditionalOperators(String line)
        {
            line = Regex.Replace(line, @"&{2}", " AND ");       //CONDITIONAL OPERATORS
            line = Regex.Replace(line, @"\|{2}", " OR ");
            line = Regex.Replace(line, @"={2}", "=");
            line = Regex.Replace(line, @"!=", "<>");
            line = Regex.Replace(line, @"<=", " <= ");
            line = Regex.Replace(line, @">=", " >= ");
            line = Regex.Replace(line, @"\b<\b", " < ");
            line = Regex.Replace(line, @"\b>\b", " > ");
            line = Regex.Replace(line, @"\b=\b", " = ");        //ASSIGNMENT OPERATOR
            line = Regex.Replace(line, @"\+", " + ");           //ARITHMETIC OPERATORS
            line = Regex.Replace(line, @"\-", " - ");
            line = Regex.Replace(line, @"/", " / ");
            line = Regex.Replace(line, @"\*", " * ");
            line = Regex.Replace(line, @"\(", " (");

            return line;    //returns the filtered line
        }


        /// <summary>
        /// Checks the given line for existing variables and swaps them with its numeric value
        /// </summary>
        /// <param name="line">line to check variables in</param>
        /// <param name="variables">variables to look for</param>
        /// <returns></returns>
        public String putVariableValues(String line, Hashtable variables)
        {
            //check if existing variable is in the line and replace it with its value
            foreach (String variable in variables.Keys)
            {
                //and or
                line = Regex.Replace(line, @"AND", " AND ");       //CONDITIONAL OPERATORS AND
                line = Regex.Replace(line, @"OR", " OR ");       //CONDITIONAL OPERATORS OR

                Match m = Regex.Match(line, @"\b" + variable + @"\b");
                if (m.Success)
                    line = Regex.Replace(line, @"\b" + variable + @"\b", variables[variable].ToString());
            }
            return line;
        }


    }//Class End
}//Namespace End
