﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Rectangle class used to define and draw rectangle shape
    /// </summary>
    /// <remarks>Inherits <see cref="abstract class Shape : Shapes"/></remarks>
    class Rectangle :Shape
    {
        /// <summary>
        /// Width of the rectangle
        /// </summary>
        int width;

        /// <summary>
        /// Height of the rectangle
        /// </summary>
        int height;

        /// <summary>
        /// Default Constructor that defines a default rectangle with size 100 by 100
        /// </summary>
        /// <remarks>Calls base constructor to define color, x and y </remarks>
        public Rectangle():base()
        {
            this.width = 100;               //default width of the rectangle
            this.height = 100;              //deafult height of the rectangle
        }

        /// <summary>
        /// Parameterized constructor that provides the color, width and height as well as cursor position of the rectangle
        /// </summary>
        /// <param name="color">Outline color for the rectangle</param>
        /// <param name="x">X position on the graphics</param>
        /// <param name="y">Y position on the graphics</param>
        /// <param name="width">Width of the rectangle</param>
        /// <param name="height">Height of the rectangle</param>
        /// <remarks>Calls base constructor to define color, x and y </remarks>
        public Rectangle(Color color, int x, int y, int width, int height):base(color, x, y)
        {
            this.width = width;                 //set the width of the rectangle
            this.height = height;               //sets the height of the rectangle
        }

        /// <summary>
        /// Sets the color, cursor position as well as the width and height of the rectangle    
        /// </summary>
        /// <param name="color">Outline color for the rectangle</param>
        /// <param name="list">Integer parameter list containing x position, y positoin and width and height of the rectangle</param>
        public override void set(Color color, params int[] list)
        {
            if ((list.Length) != 4)                                         //check and reports invalid number of parameters for this shape
                throw new InvalidParameterException("Wrong number of parameters '" + (list.Length - 2) + "' given, 2 expected for a rectangle");

            base.set(color, list[0],list[1]);                           //list[0] and list[1] being x and y coordinates 
            this.width = list[2];
            this.height = list[3];
        }

        /// <summary>
        /// Draws rectangle on the provided graphics 
        /// </summary>
        /// <param name="g">The graphics provided by the caller on which the rectangle is to be drawn</param>
        public override void draw(Graphics g)
        {
            Pen p = new Pen(base.PenColor, base.PenSize);             //uses the color and pen size defined in shape by default
            SolidBrush sb = new SolidBrush(base.FillColor);              //new solid brush to fill shapes wth the color defined in shape class by default

            //Draw Rectangle Shape around the cursor position on the provided graphics
            if(FillStatus)                                               //if fill is on
            {
                g.FillRectangle(sb, (x - width / 2), (y - height / 2), width, height);          
                g.DrawRectangle(p, (x- width / 2), (y - height / 2), width, height);
            }
            else
                g.DrawRectangle(p, (x - width / 2), (y - height / 2), width, height);
        }

        /// <summary>
        /// Calculates the surface area of the rectangle.
        /// </summary>
        /// <returns>Calculated area of the rectangle.</returns>
        public override double calcArea()
        {
            return (width * height);
        }

        /// <summary>
        /// Calculates the perimeter of the rectangle.
        /// </summary>
        /// <returns>Calculated Parameter of the rectangle.</returns>
        public override double calcPerimeter()
        {
            return (2 * width + 2 * height);
        }
    }
}
