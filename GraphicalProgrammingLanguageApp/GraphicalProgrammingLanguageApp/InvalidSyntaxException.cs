﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// InvalidSyntaxException class that occurs when the number of words typed on command exceeds the required number to be of valid syntax
    /// </summary>
    /// <remarks>Inherits <see cref="System.Exception"/></remarks>
    [Serializable]
    public class InvalidSyntaxException: System.Exception
    {
        /// <summary>
        /// String message to be displayed when thrown
        /// </summary>
        public String message;

        /// <summary>
        /// Default constructor that sets the exception message to its base class
        /// </summary>
        public InvalidSyntaxException() : base("InvalidSyntaxException: Invalid Syntax Exception") { }

        /// <summary>
        /// Parameterized constructor that takes one string message 
        /// </summary>
        /// <param name="message">string message</param>
        public InvalidSyntaxException(String message) : base(message)
        {
            this.message = message;
        }

        /// <summary>
        /// Parameterized constructor that sets the exception message to its base class and adds a additional info to the message
        /// </summary>
        ///  <param name="message">Line Number at which the exception occured</param>
        public InvalidSyntaxException(String message, int lineNo) : base(String.Format("InvalidSyntaxException: "+ message + " at Line: {0}", lineNo)) { }
    }
}
