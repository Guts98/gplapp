﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// DrawTo class used to define and draw a line
    /// </summary>
    /// <remarks>Inherits <see cref="abstract class Shape : Shapes"/></remarks>
    class DrawTo : Shape
    {
        /// <summary>
        /// destination x-cordinate of the line
        /// </summary>
        int xDes;

        /// <summary>
        /// destination y-cordinate of the line
        /// </summary>
        int yDes;

        /// <summary>
        /// Default Constructor that defines a default line to the position (100,100)
        /// </summary>
        /// <remarks>Calls base constructor to define color, x and y </remarks>
        public DrawTo() : base()
        {
            this.xDes = 100;            //set destination x position
            this.yDes = 100;            //set destinaiton y position
        }

        /// <summary>
        /// Parameterized constructor that provides the color, width and height as well as cursor position of the line
        /// </summary>
        /// <param name="color">Outline color for the line</param>
        /// <param name="x">X position on the graphics</param>
        /// <param name="y">Y position on the graphics</param>
        /// <param name="xPos">X position of the destination of the line</param>
        /// <param name="yPos">Y position of the destination of the line</param>
        /// <remarks>Calls base constructor to define color, x and y </remarks>
        public DrawTo(Color color, int x, int y, int xPos, int yPos) : base(color, x, y)
        {
            this.xDes = xPos;                 //sets the x position of the destination of the line
            this.yDes = yPos;               //sets the  y position of the destination of the line
        }

        /// <summary>
        /// Sets the color, cursor position as well as the destination points of the line    
        /// </summary>
        /// <param name="color">Outline color for the line</param>
        /// <param name="list">Integer parameter list containing starting and ending x and y positoins of the line</param>
        public override void set(Color color, params int[] list)
        {
            if ((list.Length) != 4)                             //check and reports invalid number of parameters for this shape
                throw new InvalidParameterException("Wrong number of parameters '" + (list.Length - 2) + "' given, 2 expected for a line");

            base.set(color, list[0], list[1]);           //list[0] and list[1] being x and y coordinates 
            this.xDes = list[2];
            this.yDes = list[3];
        }

        /// <summary>
        /// Draws line on the provided graphics 
        /// </summary>
        /// <param name="g">The graphics provided by the caller on which the line is to be drawn</param>
        public override void draw(Graphics g)
        {
            Pen p = new Pen(base.PenColor, base.PenSize);             //uses the color and pen size defined in shape by default
            g.DrawLine(p, base.x, base.y, this.xDes, this.yDes);        //Draw Line Shape on the provided graphics
        }

        /// <summary>
        /// Calculates the surface area of the line.
        /// </summary>
        /// <returns>Calculated area of the line.</returns>
        public override double calcArea()
        {
            //getting the line's side or hypotenuse in a right angled triangle using pythogerias theorem
            double lineLength = System.Math.Sqrt(System.Math.Pow(System.Math.Abs(this.xDes - base.x), 2) + System.Math.Pow(System.Math.Abs(this.yDes - base.y), 2));
            return (lineLength * base.PenSize);  //product of pen size and line's length
        }

        /// <summary>
        /// Calculates the perimeter of the line.
        /// </summary>
        /// <returns>Calculated Parameter of the line.</returns>
        public override double calcPerimeter()
        {
            return (System.Math.Sqrt(System.Math.Pow(System.Math.Abs(this.xDes - base.x), 2) + System.Math.Pow(System.Math.Abs(this.yDes - base.y), 2)) + base.PenSize);
        }
    }

}
