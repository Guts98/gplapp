﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Star class used to define and draw star shape
    /// </summary>
    /// <remarks>Inherits <see cref="abstract class Shape : Shapes"/></remarks>
    class Star : Shape
    {

        /// <summary>
        /// width of each side
        /// </summary>
        int side;

        /// <summary>
        /// Default Constructor that defines a default star with width 100
        /// </summary>
        /// <remarks>Calls base constructor to define color, x and y </remarks>
        public Star() : base()
        {
            this.side = 100;
        }

        /// <summary>
        /// Parameterized constructor that provides the color,  width as well as cursor position of the Star
        /// </summary>
        /// <param name="color">Outline color for the Star</param>
        /// <param name="x">X position on the graphics</param>
        /// <param name="y">Y position on the graphics</param>
        /// <param name="side">Width of each side for the Star</param>
        /// <remarks>Calls base constructor to define color, x and y </remarks>
        public Star(Color color, int x, int y, int side) : base(color, x, y)
        {
            this.side = side;                     //sets the width of the side of the Star
        }

        /// <summary>
        /// Sets the color, cursor position as well as the baseSide and height of the triangle    
        /// </summary>
        /// <param name="color">Outline color for the Star</param>
        /// <param name="list">Integer parameter list containing x position, y positoin and num of vertices and side width for the Star</param>
        public override void set(Color color, params int[] list)
        {
            if ((list.Length) != 3)                                     //check and reports invalid number of parameters for this shape
                throw new InvalidParameterException("Wrong number of parameters '" + (list.Length - 2) + "' given, 2 expected for a Star");

            base.set(color, list[0], list[1]);           //list[0] and list[1] being x and y coordinates 
            this.side = list[2];
        }

        /// <summary>
        /// Draws Star on the provided graphics 
        /// </summary>
        /// <param name="g">The graphics provided by the caller on which the triangle is to be drawn</param>
        /// <remarks>The star is drawn by deducing 10 vertices from the width provided and then drawing a Star out of those vertices.</remarks>
        public override void draw(Graphics g)
        {
            //Deducing 10 points from the proportion side given
            double a = Convert.ToDouble(this.side);
            //double b = 2 * a;
            double _x1, _x2, _x3, _x4, x5, x6, x7,x8;      //'_' means before x or y and not '_' means after it
            double _y1, _y2, _y3, y4, y5;
            _x1 = this.x - (0.5 * a);
            _x2 = this.x - (0.32 * a);
            _x3 = this.x - (0.23 * a);
            _x4 = this.x - (0.14 * a);
            x5 = this.x + (0.14 * a);
            x6 = this.x + (0.23 * a);
            x7 = this.x + (0.32 * a);
            x8 = this.x + (0.5 * a);

            _y1 = this.y - (0.64 * a);
            _y2 = this.y - (0.33 * a);
            _y3 = this.y - (0.23 * a);
            y4 = this.y + (0.18 * a);
            y5 = this.y + (0.32 * a);

            PointF p1 = new PointF(float.Parse(this.x.ToString()),float.Parse(_y1.ToString()));
            PointF p2 = new PointF(float.Parse(x5.ToString()), float.Parse(_y2.ToString()));
            PointF p3 = new PointF(float.Parse(x8.ToString()), float.Parse(_y3.ToString()));
            PointF p4 = new PointF(float.Parse(x6.ToString()), float.Parse(this.y.ToString()));
            PointF p5 = new PointF(float.Parse(x7.ToString()), float.Parse(y5.ToString()));
            PointF p6 = new PointF(float.Parse(this.x.ToString()), float.Parse(y4.ToString()));
            PointF p7 = new PointF(float.Parse(_x2.ToString()), float.Parse(y5.ToString()));
            PointF p8 = new PointF(float.Parse(_x3.ToString()), float.Parse(this.y.ToString()));
            PointF p9 = new PointF(float.Parse(_x1.ToString()), float.Parse(_y3.ToString()));
            PointF p10 = new PointF(float.Parse(_x4.ToString()), float.Parse(_y2.ToString()));

            Pen p = new Pen(base.PenColor, base.PenSize);             //uses the color and pen size defined in shape by default
            SolidBrush sb = new SolidBrush(base.FillColor);              //new solid brush to fill shapes wth the color defined in shape class by default

            PointF[] vertices = { p1, p2, p3,p3,p4,p5,p6,p7,p8,p9,p10 };

            //Draw Triangle Star Shape on the provided graphics
            if (FillStatus)                             //if fill is on
            {
                g.DrawPolygon(p, vertices);         //draw hollow triangle
                g.FillPolygon(sb, vertices);        //draw filled triangle
            }
            else
                g.DrawPolygon(p, vertices);         //draw hollow triangle         
        }

        /// <summary>
        /// Calculates the surface area of the Star.
        /// </summary>
        /// <returns>Calculated area of the Star.</returns>
        public override double calcArea()
        {
            return 0;// ((numVertex * side) / 2);
        }

        /// <summary>
        /// Calculates the perimeter of the Star.
        /// </summary>
        /// <returns>Calculated Parameter of the Star.</returns>
        public override double calcPerimeter()
        {
            //double s1, s2, s3 = this.numVertex;
            //s1 = s2 = (numVertex + System.Math.Sqrt(System.Math.Pow(this.numVertex / 2, 2) + System.Math.Pow(this.side / 2, 2)));

            return 0;// (s1 + s2 + s3);
        }
    }
}
