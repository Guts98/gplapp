﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Method class which stores information of a defined method like statements, starting line, method signature, method 
    /// arguments, etc in and as an object 
    /// </summary>
    public class Method
    {
        /// <summary>
        /// The method signature or its declaration statement
        /// </summary>
        private String methodSignature;

        /// <summary>
        /// The name of the method
        /// </summary>
        private String methodName;

        /// <summary>
        /// Flag value for if the method has any parameters or not
        /// </summary>
        private bool hasParameters;

        /// <summary>
        /// The parameters array that the method takes in
        /// </summary>
        private String[] methodParameters;

        /// <summary>
        /// The statements inside the method body
        /// </summary>
        private ArrayList statements;

        /// <summary>
        /// Line number of the line where method signature is declared
        /// </summary>
        private int methodSignatureLineNum;

        /// <summary>
        /// The parameter values provided during method call
        /// </summary>
        private String[] calledParameters;

        /// <summary>
        /// Load the method statements into a new array for temporary use where 
        /// some tokens might be replaced with passed parameters
        /// </summary>
        private String[] executableStatements;

        /// <summary>
        /// Getter Setter for methodSignature
        /// </summary>
        public string MethodSignature { get => methodSignature; set => methodSignature = value; }

        /// <summary>
        /// Getter Setter for methodName
        /// </summary>
        public string MethodName { get => methodName; set => methodName = value; }

        /// <summary>
        /// Getter Setter for hasParameters
        /// </summary>
        public bool HasParameters { get => hasParameters; set => hasParameters = value; }

        /// <summary>
        /// Getter Setter for methodParameters
        /// </summary>
        public string[] MethodParameters { get => methodParameters; set => methodParameters = value; }

        /// <summary>
        /// Getter Setter for statements
        /// </summary>
        public ArrayList Statements { get => statements; set => statements = value; }

        /// <summary>
        /// Getter Setter for calledParameters
        /// </summary>
        public string[] CalledParameters { get => calledParameters; set => calledParameters = value; }

        /// <summary>
        /// Getter Setter for executableStatements
        /// </summary>
        public string[] ExecutableStatements { get => executableStatements; set => executableStatements = value; }

        /// <summary>
        /// Getter Setter for methodSignatureLineNum
        /// </summary>
        public int MethodSignatureLineNum { get => methodSignatureLineNum; set => methodSignatureLineNum = value; }


        /// <summary>
        /// Default constructor for method class object
        /// </summary>
        public Method()
        {
            Console.WriteLine("Method object created!");
            statements = new ArrayList();               //initialize the arraylist to store the method body's statements
        }

        /// <summary>
        /// Adds then passed statement to the method block for later execution
        /// </summary>
        /// <param name="statement">statement to be added</param>
        public void addStatement(String statement)
        {
            Statements.Add(statement);
        }

        /// <summary>
        /// Filters and breaks down the method call syntax, thereafter handles the cases for no parameters or for with
        /// parameters method call, replaces the local parameters from method defined signature with the passed values 
        /// or pre-defined variables..and also prepares a final block of statements for execution
        /// </summary>
        /// <param name="exeSignature">Method call signature like: myMethod(param1, param2) or myMethod()</param>
        /// <param name="variables">The variables hashtable for comparing and storing relevant datas</param>
        public void filterMethodExecutionSignature(String exeSignature, Hashtable variables)
        {
            //expected syntax myMethod(param1, param2,..) or myMethod() for without parameters
            String[] splitLine = exeSignature.Split(' ');
            if (splitLine.Length < 2)
            {
                //throw invalid syntax exception(in case only keyword method was found)
                Console.WriteLine("Invalid method declaration syntax");
                throw new MethodCallException("Bad method call syntax");
            }
            //String calledMethodName;// = splitLine[0];

            //get the method name and params which could be either of these: myMethod() or myMethod(params....)
            String methodNameParameters = CommandParser.splitArrayToString(splitLine, 1);     //removes the possible space between method name and parameters

            //Confirms and handles the presence of '(' and ')' brackets in the signature
            MatchCollection openBracket = Regex.Matches(this.MethodSignature, @"[(]");
            MatchCollection closeBracket = Regex.Matches(this.MethodSignature, @"[)]");
            if ((openBracket.Count != 1) || (closeBracket.Count != 1))
            {
                //throw invalid syntax exception(in case more than 1 ( or ) was found)
                Console.WriteLine("Invalid method declaration syntax");
                throw new MethodCallException("Missing '(' or ')' or were wrongly distributed");
            }
            else if (closeBracket[0].Index != (this.MethodSignature.Count() - 1))
            {
                //throw invalid syntax exception(in case ) was not at the last index)
                Console.WriteLine("Invalid method declaration syntax");
                throw new MethodCallException("Missing '(' or ')' or were wrongly distributed");
            }

            //split the name and parameters separately
            String[] splitLine2 = methodNameParameters.Split('(');  //this array length will always be 2 as only 1 ( was verified earlier

            String stringParameters = splitLine2[1];           //here the signature has been broken to " param1,param2) " or " ) " if no parameters
            stringParameters = Regex.Replace(stringParameters, @"\)", String.Empty); //remove the ) from parameters list

            //Now compare parameter list from called method and original method signature
            this.CalledParameters = stringParameters.Split(',');
            if (this.CalledParameters.Length == 1 && !this.hasParameters)
            {
                if (!this.CalledParameters[0].Trim().Equals(""))
                {
                    //throw error parameters were passed to a method signature that doesn't take any!
                    Console.WriteLine("Error parameters were passed to a method signature that doesn't take any!");
                    throw new MethodCallException("The called method doesn't take any parameters");
                }
            }
            else if (this.CalledParameters.Length != this.methodParameters.Length)
            {
                //throw error number of parameters didn't match with the method signature
                Console.WriteLine("Error number of parameters didn't match with the method signature!");
                throw new MethodCallException("Wrong number of parameters passed");
            }
            else { }

            //Load the method statements into a new array for temporary use where some tokens might be replaced with passed parameters
            this.ExecutableStatements = new String[this.Statements.Count];
            int k = 0;
            foreach (String statement in this.Statements)
            {
                this.ExecutableStatements[k] = statement;
                k++;
            }
            if(this.HasParameters)
            { 
                int i = 0;
                foreach (String parameter in this.methodParameters)
                {
                    this.CalledParameters[i] = CalledParameters[i].Trim();
                    int num;
                    bool intSuccess = Int32.TryParse(CalledParameters[i].Trim(), out num);
                    if (variables.ContainsKey(CalledParameters[i]))          //if the variable hashtable contains the provided variable
                    {
                        int j = 0;
                        foreach (String statement in this.ExecutableStatements)     //remove the parameter of this name with the variable name
                        { 
                            this.ExecutableStatements[j] = Regex.Replace(this.ExecutableStatements[j], @"\b" + parameter + @"\b", this.CalledParameters[i]);
                            j++;
                        }
                        i++;
                        continue;
                    }
                    else if (intSuccess)             //if the suplied parameter was an integer, then replcae the parameter names with the value
                    {
                        int j = 0;
                        foreach (String statement in this.ExecutableStatements)
                        {
                            this.ExecutableStatements[j] = Regex.Replace(this.ExecutableStatements[j], @"\b" + parameter + @"\b", num.ToString());
                            j++;
                        }
                        i++;
                        continue;
                    }
                    else
                    {
                        //throw error invalid parameter provided in method call
                        i++;
                        Console.WriteLine(" error invalid parameter provided in method call");
                        throw new MethodCallException("Invalid parameters passed ");
                    }
                }
            }
            //if it reaches here means the method call signature was successfully filtered
            Console.WriteLine("Method Call Signature filtered successfully");
        }

        ///<summary>
        /// Filters and breaks down the method defining syntax, thereafter handles the cases for no parameters or for with
        /// parameters method call, replaces the local parameters from method defined signature with the passed values 
        /// or pre-defined variables..and also prepares a final block of statements for execution
        /// </summary>
        /// <param name="storedMethods">Container of the list of method objects that already exists</param>
        public void filterSignature(Hashtable storedMethods)
        {
            //Valid Signatures: "method myMethod(param1, param2)" or "method myMethod()" if no parameters

            //separate "method" and get the myMethod(param1, param2)
            String[] splitLine = this.MethodSignature.Split(' ');

            if(splitLine.Length < 2)
            {
                //throw invalid syntax exception(in case only keyword method was found)
                Console.WriteLine("Invalid method declaration syntax");
                throw new MethodDeclarationException("Method name couldn't be identified");
            }

            //get the method name and params which could be either of these: myMethod() or myMethod(params....)
            String methodNameParameters = CommandParser.splitArrayToString(splitLine, 1);     //removes the possible space between method name and parameters

            //Confirms and handles the presence of '(' and ')' brackets in the signature
            MatchCollection openBracket = Regex.Matches(this.MethodSignature, @"[(]");
            MatchCollection closeBracket = Regex.Matches(this.MethodSignature, @"[)]");
            if((openBracket.Count != 1) || (closeBracket.Count != 1))
            {
                //throw invalid syntax exception(in case more than 1 ( or ) was found)
                Console.WriteLine("Invalid method declaration syntax");
                throw new MethodDeclarationException("Missing '(' or ')' or were wrongly distributed");
            }
            else if(closeBracket[0].Index != (this.MethodSignature.Count() - 1))
            {
                //throw invalid syntax exception(in case ) was not at the last index)
                Console.WriteLine("Invalid method declaration syntax");
                throw new MethodDeclarationException("Missing '(' or ')' or were wrongly distributed");
            }

            //split the name and parameters separately
            String[] splitLine2 = methodNameParameters.Split('(');  //this array length will always be 2 as only 1 ( was verified earlier

            //method name
            this.MethodName = splitLine2[0];

            //Checks if the new method name is valid
            if (!CommandParser.validateTokenName(this.MethodName.Trim()))
                throw new InvalidTokenException(this.MethodName.Trim());

            String stringParameters = splitLine2[1];           //here the signature has been broken to " param1,param2) " or " ) " if no parameters
            stringParameters = Regex.Replace(stringParameters, @"\)", String.Empty); //remove the ) from parameters list

            //now check if there were no parameters if nothing is left of the signature
            if (stringParameters.Trim().Equals(""))
                this.HasParameters = false;
            else
                this.HasParameters = true;

            //if it indeed has parameters
            if(this.HasParameters)
            {
                storeParameters(stringParameters);
            }

            //check if method name alread exists
            if(methodExists(storedMethods, this.MethodName))
            {
                //throw error 
                Console.WriteLine("Method name is already exists!");
                throw new MethodDeclarationException("Method name already exists");
            }

            //If it reaches to this point, it means the method signature was successfully filtered
            Console.WriteLine("Method Filtering Success!");
        }

        /// <summary>
        /// Store the parameters from method signature during method definition 
        /// </summary>
        /// <param name="stringParameters">parameters to be  </param>
        public void storeParameters(String stringParameters)
        {
            //split parameters by ','
            this.MethodParameters = stringParameters.Split(',');

            //validate parameter/variable names
            int i = 0;
            foreach (String param in this.MethodParameters)
            {
                if (!CommandParser.validateTokenName(param.Trim()))
                    throw new InvalidTokenException(param.Trim());
                else
                    this.MethodParameters[i] = param.Trim();                //trim the parameter/variable name
                    i++;
            }
        }

        /// <summary>
        /// Checks if the method already exists to halt its redefinition
        /// </summary>
        /// <param name="storedMethods">The Collection of method objects that already exists</param>
        /// <param name="methodName">name of the method to check for</param>
        /// <returns></returns>
        public static bool methodExists(Hashtable storedMethods, String methodName)
        {
            if (storedMethods.ContainsKey(methodName))
                return true;
            return false;
        }

        /// <summary>
        /// Custom toString() method that returns a string object to display the data of this object
        /// </summary>
        /// <returns>String content of the object</returns>
        public  string toString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Method Signature: " + this.methodSignature + "\r\n");
            sb.Append("No. of statements: " + statements.Count+"\r\n");
            sb.Append("Method Name: " + this.methodName+ "\r\n");
            sb.Append("Has Parameters: " + HasParameters + "\r\n");
            sb.Append("Statements:   \r\n");int i = 1;
            foreach(String s in statements)
            {
                sb.Append("Statement "+(i++)+": " + s + "\r\n");
            }
            return sb.ToString();
        }

    }
}
