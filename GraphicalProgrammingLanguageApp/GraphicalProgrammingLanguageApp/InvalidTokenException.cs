﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// InvalidPenSizeException class that occurs when the size for pen is not provided in the range of 1 to 20 
    /// </summary>
    /// <remarks>Inherits <see cref="System.Exception"/></remarks>
    [Serializable]
    public class InvalidTokenException : System.Exception
    {
        /// <summary>
        /// String message to be displayed when thrown: here this message is the invalid token name defined
        /// </summary>
        public String message;


        /// <summary>
        /// Default constructor that sets the exception message to its base class
        /// </summary>
        public InvalidTokenException() : base("Invalid Token Name.") { }

        /// <summary>
        /// Parameterized constructor that takes one string message 
        /// </summary>
        /// <param name="message">string message</param>
        public InvalidTokenException(String message) : base("InvalidTokenException: Invalid token name '" + message.Trim() + "' in variable, parameter or method declaration")
        {
            this.message = message;
        }

        /// <summary>
        /// Parameterized constructor that sets the exception message to its base class and adds a additional info to the message
        /// </summary>
        ///  <param name="message">Line Number at which the exception occured</param>
        public InvalidTokenException(String message, int lineNo) : base(String.Format("InvalidTokenException: Invalid token name '" + message.Trim() + "' in variable, parameter or method declaration at Line: {0}", lineNo)) { }
    }
}
