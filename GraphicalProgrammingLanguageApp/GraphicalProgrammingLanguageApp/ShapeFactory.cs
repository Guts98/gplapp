﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// ShapeFactory class that produces and returns new objects of the different shapes
    /// </summary>
    class ShapeFactory
    {
        /// <summary>
        /// Returns the shape that's requested through the parameter
        /// </summary>
        /// <param name="shapeType">Name of the shape whose object needs to be created</param>
        /// <returns></returns>
        public Shape getShape(String shapeType)
        {
            shapeType = shapeType.ToUpper().Trim();
            if (shapeType.Equals("STAR"))
            {
                return new Star();
            }
            else if (shapeType.Equals("CIRCLE"))
            {
                return new Circle();
            }
            else if (shapeType.Equals("TRIANGLE"))
            {
                return new Triangle();

            }
            else if (shapeType.Equals("SQUARE"))
            {
                return new Square();
            }
            else if(shapeType.Equals("RECT"))
            {
                return  new Rectangle();
            }
            else if (shapeType.Equals("DRAWTO"))
            {
                return new DrawTo();
            }
            else if (shapeType.Equals("POLYGON"))
            {
                return new Polygon();
            }
            else
            {
                Console.WriteLine("Factory Failed!");
                throw new InvalidCommandException("Invalid command '"+shapeType+"' encountered");
                
            }
            
        }
    }
}
