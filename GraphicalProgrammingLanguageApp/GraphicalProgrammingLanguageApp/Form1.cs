﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// GPLForm class which defines the contents of the main application form 
    /// </summary>
    /// <remarks>Partially Inherits <see cref="System.Windows.Forms"/></remarks>
    public partial class GPLForm : Form
    {
        /// <summary>
        /// Bitmap to be displayed on the output picture box
        /// </summary>
        Bitmap outputBitmap = new Bitmap(985, 460);         //740,400

        /// <summary>
        /// bitmap to display cursor point also to be displayed on the output picturebox
        /// </summary>
        Bitmap cursorBitmap = new Bitmap(985, 460);

        /// <summary>
        /// Canvas reference to draw on the bitmap graphics
        /// </summary>
        Canvas outputCanvas;


        /// <summary>
        /// Stores all parse exceptions during execution of the command
        /// </summary>
        public StringBuilder parseExceptions;

        /// <summary>
        /// number of errors encountered while parsing
        /// </summary>
        public int errorCount;

        /// <summary>
        /// Checks if multicommandline was requested to run
        /// </summary>
        public static bool multiCommandRun;

        /// <summary>
        /// Stores the path of the file currently loaded
        /// </summary>
        String loadedfilePath;

        /// <summary>
        /// Stores the name of the file currently loaded
        /// </summary>
        String loadedFileName;

        /// <summary>
        /// List of all the shapes created in one program
        /// </summary>
        ArrayList shapesList;

        /// <summary>
        /// Circle reference that points to the cursor object to be drawn on the canvas
        /// </summary>
        Circle cursor;

        /// <summary>
        /// Checks the syntax of the program if this var is set to true which is done when syntaxcheck button is clicked
        /// </summary>
        bool syntaxToCheck;

        /// <summary>
        /// Reference to the command parser object to parse the commands
        /// </summary>
        CommandParser cmdParser;

        /// <summary>
        /// Program queue reference that stores valid draw program lines in a queue fifo format
        /// </summary>
        ProgramQueue programQueue;

        /// <summary>
        /// Reference that takes one draw command from program command at a time and parses and draws it on the canvas
        /// </summary>
        ExecutableStack exeStack;

        /// <summary>
        /// Stores the variables in ke value format
        /// </summary>
        Hashtable variables;

        Thread threadParse;

        /// <summary>
        /// Default constructor for the GPLForm class
        /// </summary>
        public GPLForm()
        {

            InitializeComponent();
            outputCanvas = new Canvas(Graphics.FromImage(outputBitmap));  //class for handling the drawing, pass the drawing surface to it
            cursor = new Circle();
            updateCursor(Graphics.FromImage(cursorBitmap));               //updates the cursor position of the canvas
            multiCommandRun = false;                                    //initially no request is made to run multi command lines

            //by default no file's been loaded
            this.loadedfilePath = "";
            this.loadedFileName = "";

            penColorIndicator.Text = Convert.ToString(outputCanvas.pen.Width);      //set pen indicator
            syntaxToCheck = false;                                      //by default this syntax checker is set to false and only set true when syntax check button is clicked

            //set brush indicator
            if (outputCanvas.fillStatus)
                brushColorIndicator.Text = "on";
            else
                brushColorIndicator.Text = "Off";
        }


        /// <summary>
        /// Action listener when the form loads initially
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void GPLForm_Load(object sender, EventArgs e)
        {
            //commandLine.TabIndex = 0;
            //commandLine.Focus();
        }

        /// <summary>
        /// Creates a new object of command parser and calls its parser method to parse the lines of the text box one by one
        /// and stores the drawable lines in a new queue which is again later executed one by one in the sequential order that 
        /// they were stored in
        /// </summary>
        public void executeRun()
        //public void executeRun(CommandParser cmdParser, Hashtable variables, ProgramQueue programQueue, StringBuilder parseExceptions)
        {
            cmdParser = new CommandParser();     //new command parser object to parse the commands via its method
            variables = new Hashtable();        //new variables hashtable to store the variables
                                                //new program queue that stores the draw commands after evaluation in command parser
            programQueue = new ProgramQueue();

            int loop = 1;
            int lineNo = 1;                          //set the line no to 1
            errorCount = 0;                          //reset error count for every new multi line program
            parseExceptions = new StringBuilder();  //new object to store errors for everytime a button is clicked
            if (commandLine.Text.Trim().ToLower().Equals("run"))
            {
                /******************************************************************/
                /***** ITERATES THROUGH THE LINES OF THE MULTI COMMAND LINE ******/
                /****************************************************************/
                foreach (String pline in multiCommandLine.Lines)
                {
                    Console.WriteLine("loopNo: " + loop++);
                    //this line will now be executed inside the run method where errors are handled 
                    run("cmdparser", pline, lineNo++);
                }
            }
            else  /***** PARSES THE SINGLE LINE COMMAND ******/
            {
                Console.WriteLine("loopNo: " + loop++);
                //this line will now be executed inside the run method where errors are handled 
                run("cmdparser", commandLine.Text, lineNo++);
            }
            /**************************************************************/
            /************ MAIN DRAW COMMAND EXECUTION BLOCK **************/
            /************************************************************/
            if (programQueue.size() > 0)    //if there are draw commands stored in queue
            {
                Console.WriteLine("Reached queue execution block! Line Numbers: ");
                programQueue.displaylineNums();
                shapesList = new ArrayList();           //initialize the shapelist collection
                int count = 0;
                do
                {
                    Console.WriteLine("loopNo: " + loop++);
                    exeStack = new ExecutableStack(); //new stack object for storing the line to be executed
                    int lineNum = programQueue.getLineNum(count);   //line number of the command statement
                    Console.WriteLine("queueSize: " + programQueue.size() + " varSize: " + variables.Count + " line: " + programQueue.peek() + "lineNo:" + lineNum);
                    exeStack.push(programQueue.dequeue());  //add the line at top of queue to the executable stack
                    String firstWord = ((exeStack.peek().ToString()).Split(' '))[0]; //first token
                    ////if the first token matches a valid draw command, tho whis check was done several times before
                    if (new DrawCommands().contains(firstWord))
                    {
                        //If syntax check button is not clicked, and the requested is from run button
                        if (!syntaxToCheck)
                        {
                            Console.WriteLine("reached inside stack execution block");
                            //this line will now be executed inside the run method where errors are handled 
                            run("drawparser", (String)exeStack.peek(), lineNum, outputCanvas);
                            while (shapesList.Count > 0)        //if any shapes were added in this line
                            {
                                /*******************DRAWS THE SHAPE ON THE CANVAS***********************/
                                Shape shape = (Shape)shapesList[0];
                                shape.draw(outputCanvas.graphics);  //call the draw method of shape class
                                shapesList.Clear();     //clear the shapelist arraylist for next iteration
                            }
                        }
                        else
                        {
                            /*** PARSES THE DRAW COMMANDS ON A NEW CANVAS OBJECT FOR SNTAX CHECKING ***/
                            run("drawparser", (String)exeStack.peek(), lineNum, new Canvas());
                        }
                    }
                    count++;
                    programQueue.enqueue(exeStack.pop());       //add the line back to the end of queue
                    Refresh();
                    //Thread.Sleep(1);
                } while (count < programQueue.size());
            }
            refreshAll(); //Refreshes the application display such as its canvas, error display, paint brush labels, etc all at once
        }

        /// <summary>
        /// Runs the commands from the command line by parsing and executing it
        /// </summary>
        /// <param name="command"></param>
        /// <param name="lineNo"></param>
        // public void run(String command, int lineNo)

        /// <summary>
        /// Calls the parse methods of command parser and draw commands, while catching and
        /// handling all the exceptions in case any thrown
        /// </summary>
        /// <param name="callerIdentifier">String identifier that determines if parser from command
        /// parser or draw parser is to be executed</param>
        /// <param name="parameters">object array that minimally holds command statement and its line number</param>
        public void run(String callerIdentifier, params Object[] parameters)
        {
            String command = "";  //by deafult
            int lineNo = 0;     //by default
            try
            {
                if (callerIdentifier.Equals("cmdparser"))       //If the control command is being parsed
                {
                    command = parameters[0].ToString();         //command line 
                    lineNo = Convert.ToInt32(parameters[1]);    //line number of the command line
                    /******* HERE COMMANDS FROM COMMAND TEXT BOX ARE PARSED ******/
                    cmdParser.parseCommand(programQueue, command, variables, lineNo);
                }
                else if (callerIdentifier.Equals("drawparser"))       //If the draw command is being parsed
                {
                    command = parameters[0].ToString();         //command line
                    lineNo = Convert.ToInt32(parameters[1]);    //line number of the command line
                    Canvas canvas = (Canvas)parameters[2];      //Canvas object to apply the parse commands on
                    /****************** HERE DRAW COMMANDS ARE PARSED ****************/
                    new DrawCommands().parseCommand((String)exeStack.peek(), canvas, shapesList, variables);
                }
                else { }
            }
            catch (MethodDeclarationException e)                                     //error in method declaration
            {
                errorCount++;
                MethodDeclarationException ex = new MethodDeclarationException(e.message, lineNo);
                parseExceptions.Append(ex.Message + "\n");
                parseExceptions.Append("----------------------------------------------------------" +
                    "------------------------------------------------------------------------------\n");
            }
            catch (MethodCallException e)                                     //error in method declaration
            {
                errorCount++;
                MethodCallException ex = new MethodCallException(e.message, lineNo);
                parseExceptions.Append(ex.Message + "\n");
                parseExceptions.Append("----------------------------------------------------------" +
                    "------------------------------------------------------------------------------\n");
            }
            catch (InvalidTokenException e)                                     //error in method declaration
            {
                errorCount++;
                InvalidTokenException ex = new InvalidTokenException(e.message, lineNo);
                parseExceptions.Append(ex.Message + "\n");
                parseExceptions.Append("----------------------------------------------------------" +
                    "------------------------------------------------------------------------------\n");
            }
            catch (ComputationalException e)                                     //error in method declaration
            {
                errorCount++;
                ComputationalException ex = new ComputationalException(e.message, lineNo);
                parseExceptions.Append(ex.Message + "\n");
                parseExceptions.Append("----------------------------------------------------------" +
                    "------------------------------------------------------------------------------\n");
            }
            catch (InvalidCommandException e)                                            //when command name is invalid
            {
                errorCount++;
                InvalidCommandException ex = new InvalidCommandException(e.message, lineNo);
                parseExceptions.Append(ex.Message + "\n");
                parseExceptions.Append("----------------------------------------------------------" +
                    "------------------------------------------------------------------------------\n");
            }
            catch (InvalidSyntaxException e)                                            //when the overall syntax is wrong
            {
                errorCount++;
                InvalidSyntaxException ex = new InvalidSyntaxException(e.message, lineNo);
                parseExceptions.Append(ex.Message + "\n");
                parseExceptions.Append("----------------------------------------------------------" +
                    "------------------------------------------------------------------------------\n");
            }
            catch (InvalidParameterException e)                                          //when the format of the parameter is wrong
            {
                errorCount++;
                InvalidParameterException ex = new InvalidParameterException(e.message, lineNo);
                parseExceptions.Append(ex.Message + "\n");
                parseExceptions.Append("----------------------------------------------------------" +
                     "------------------------------------------------------------------------------\n");
            }
            catch (InvalidPenSizeException e)                                    //then the size of the pen is not within given range
            {
                errorCount++;
                InvalidPenSizeException ex = new InvalidPenSizeException(e.message, lineNo);
                parseExceptions.Append(ex.Message + "\n");
                parseExceptions.Append("----------------------------------------------------------" +
                    "------------------------------------------------------------------------------\n");
            }
            catch (InvalidColorException e)                                    //when the color specified is not valid
            {
                errorCount++;
                InvalidColorException ex = new InvalidColorException(e.message, lineNo);
                parseExceptions.Append(ex.Message + "\n");
                parseExceptions.Append("----------------------------------------------------------" +
                    "------------------------------------------------------------------------------\n");
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception Message: " + e.Message + "at line:" + lineNo);
                parseExceptions.Append("Exception: Error parsing line " + lineNo +
                    "\n----------------------------------------------------------" +
                    "------------------------------------------------------------------------------\n");
            }
        }

        /// <summary>
        /// Refreshes the application display such as its canvas, error display, paint brush labels, etc all at once
        /// </summary>
        public void refreshAll()
        {
            //updateErrorDisplay();                                               //Updates the text shown in the error display text box
            updateCursor(Graphics.FromImage(cursorBitmap));                     //updates the cursor position
            refreshPenBrushIndicators();                                        //refresh the brush and pen indicators
            errorDisplay.Text = parseExceptions.ToString();                     //converts string builder object to string
            updateErrorDisplay();                                               //Updates the text shown in the error display text box
                                                                                // SyntaxColorRTB sc = new SyntaxColorRTB();
                                                                                // sc.colorSyntax(errorDisplay);

            Refresh();                                                          //signify that displayneeds updating
        }


        /// <summary>
        /// Updates the text shown in the error display text box
        /// </summary>
        public void updateErrorDisplay()
        {
            if (errorCount == 0)
            {
                errorDisplay.Text = "No Errors Found";               //if parsed successfully, add no error found message to error display
                //sets font color of error display to green if no error found
                errorDisplay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(222)))), ((int)(((byte)(111)))));
                errorHeaderLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(222)))), ((int)(((byte)(111)))));
            }
            else
            {
                //sets font color of error display to red if error found
                errorDisplay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(60)))), ((int)(((byte)(26)))));
                errorHeaderLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(146)))), ((int)(((byte)(51)))));
            }
            errorHeaderLabel.Text = errorCount + " Errors";
            cursorPositionInfo.Text = "x:" + outputCanvas.XPos + ", y:" + outputCanvas.YPos;

        }


        /// <summary>
        /// Draws cursor on the canvas 
        /// </summary>
        /// <param name="g">Graphics on which the cursor is to be drawn</param>
        public void updateCursor(Graphics g)
        {
            //sets same coordinates as the current point's position in the output canvas
            int x = outputCanvas.XPos;
            int y = outputCanvas.YPos;

            g.Clear(Color.Transparent);                           //clears and makes a fresh transparent bitmap off cursor bitmap graphics

            //set cursor circle properties
            int cursorRadius = 4;
            cursor.FillStatus = true;
            cursor.FillColor = Color.Red;
            cursor.PenSize = 1;
            cursor.PenColor = Color.Red;
            cursor.set(Color.Red, new int[] { x, y, cursorRadius });    //color could also be set same as the pen's color but by default it stays red all the time

            cursor.draw(g);                                     //draw cursor on the cursor canvas
        }

        /// <summary>
        /// Refreshes the fill brush indicators on the GUI
        /// </summary>
        public void refreshPenBrushIndicators()
        {
            penColorIndicator.Text = Convert.ToString(outputCanvas.pen.Width);      //set pen indicator 

            if (outputCanvas.fillStatus)                                            //set fill indicator
                brushColorIndicator.Text = "on";
            else
                brushColorIndicator.Text = "Off";

            outputCanvas.updateColorIndicators();                           //updateColorIndicatorsof the canvas, is optional to be called from here
        }

        /// <summary>
        /// Paints action to be performed on output screen picture box
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void outputScreen_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;                                    //get graphics context of form (which is being displayed)

            g.DrawImageUnscaled(outputBitmap, 0, 0);                    //put the off screen bitmap on the picture box
            g.DrawImageUnscaled(cursorBitmap, 0, 0);                    //put the cursor bitmap on the picture box
        }

        /// <summary>
        /// Action listener for when exit menu item is clicked
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void exitMenu_Click(object sender, EventArgs e)
        {
            this.Close();                                                   //closes the form window
        }

        /// <summary>
        /// action listener when enter is pressed while inside the single command line
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void commandLine_KeyDown(object sender, KeyEventArgs e)
        {
            //useSyntaxColoring(commandLine);            //use syntax coloring on single command line
            if (e.KeyCode == Keys.Enter)                                     //if enter key is pressed
            {
               // stopLoopExecution = false;
                executeRun();
            }
        }

        /// <summary>
        /// Action listener when the run button is clicked
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void runBtn_Click(object sender, EventArgs e)
        {
            executeRun();
        }

        /// <summary>
        /// Action listener button for when syntax check button is clicked
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void syntaxBtn_Click(object sender, EventArgs e)
        {
            //sets syntax checking property to true so when run method is called, it runs all the commands
            //on a separate test canvas and not the real output 
            syntaxToCheck = true;
            executeRun();
            syntaxToCheck = false;
        }

        /// <summary>
        /// Action Listener for when load menu button is clicked
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void loadMenuItem_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "d:\\";
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        fileContent = reader.ReadToEnd();
                        multiCommandLine.Text = fileContent;
                        fileNameLabel.Text = Path.GetFileName(openFileDialog.FileName);     //display the file name to the file name label

                        //store the file's name and path to the object's internal instance variables
                        this.loadedFileName = Path.GetFileName(openFileDialog.FileName); ;
                        this.loadedfilePath = openFileDialog.FileName;
                    }
                }
            }
        }

        /// <summary>
        /// Writes the contents of multi command line to the file bieing saved
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void saveMenuItem_Click(object sender, EventArgs e)
        {
            //Stream myStream;
            StreamWriter fWriter;                                                   //streamwriter to write to the file
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();                  //open save file dialog

            saveFileDialog1.Title = "Save Gpl Text Files";                            //save dialog title extension
            saveFileDialog1.DefaultExt = ".gpl.txt";                                     //default extension  

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";     //gives save format options
            saveFileDialog1.FilterIndex = 1;

            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //write the contents of multi command line text box to file being saved
                fWriter = File.CreateText(saveFileDialog1.FileName);
                fWriter.Write(multiCommandLine.Text);
                fWriter.Close();

                //Sets the file name to the file name label
                if (!fileNameLabel.Text.Equals(Path.GetFileName(saveFileDialog1.FileName)))
                {
                    fileNameLabel.Text = Path.GetFileName(saveFileDialog1.FileName);           //display the file name to the file name label

                    //store the file's name and path to the object's internal instance variables
                    this.loadedFileName = Path.GetFileName(saveFileDialog1.FileName);
                    this.loadedfilePath = saveFileDialog1.FileName;
                }
            }
        }

        /// <summary>
        /// When the close button is clicked to close the opening file and delete its contents from multi command line
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void fileCloseBtn_Click(object sender, EventArgs e)
        {
            if (!fileNameLabel.Equals("Untitled.gpl.txt"))
            {
                fileNameLabel.Text = "Untitled.gpl.txt";
                multiCommandLine.Text = "";
                this.loadedfilePath = "";
                this.loadedFileName = "";
            }
        }

        /// <summary>
        /// Decides whether to add editing symbol i.e. * to the end of the file name label if a file is being loaded
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void multiCommandLine_KeyDown(object sender, KeyEventArgs e)
        {
            if (!this.loadedfilePath.Equals(""))
            {
                // Stores the file's content to a variable
                string fileText = File.ReadAllText(this.loadedfilePath);

                //Compare the cotent of multiline command and file's content
                if (!fileText.Equals(multiCommandLine.Text))
                    this.fileNameLabel.Text = this.loadedFileName + "*";
                else
                    fileNameLabel.Text = this.loadedFileName;
            }
        }

        /// <summary>
        /// changes font color of file menu when mouse enters
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void fileMenu_MouseEnter(object sender, EventArgs e)
        {
            fileMenu.ForeColor = Color.Black;
        }

        /// <summary>
        /// changes font color of file menu when mouse leaves
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void fileMenu_MouseLeave(object sender, EventArgs e)
        {
            fileMenu.ForeColor = Color.WhiteSmoke;
        }

        /// <summary>
        /// changes font color of help menu when mouse enters
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void helpMenu_MouseEnter(object sender, EventArgs e)
        {
            helpMenu.ForeColor = Color.Black;
        }

        /// <summary>
        /// changes font c
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void helpMenu_MouseLeave(object sender, EventArgs e)
        {
            helpMenu.ForeColor = Color.WhiteSmoke;
        }

        /// <summary>
        /// Opens syntax glossary i.e. a new form showing all the valid commands
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void syntaxGlossaryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SyntaxHelpForm helpForm = new SyntaxHelpForm();
            helpForm.ShowDialog();
        }

        /// <summary>
        /// Displays a message box showing about the author
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // About message dialog
            String about = "Name: Amit K. Karn \r\n" +
                "Student ID: c7202634 \r\n" +
                "Module: Advanced Software Engineering \r\n" +
                "Module Tutor: Mr. Resham Pun \r\n" +
                "Module Leader: Dr. Duncan Muller \r\n" +
                "Application Name: Graphical Programming Language Application";
            MessageBox.Show(about, "About GPL App");
        }

        /// <summary>
        /// Saves the image drawn on output bitmap as image file
        /// </summary>
        /// <param name="sender">Reference to the control/object that raised the event</param>
        /// <param name="e">Event Data</param>
        private void saveAsImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "JPF(.JPG)|.jpg";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                outputBitmap.Save(saveFileDialog.FileName);
                MessageBox.Show("Bitmap successfully saved as image!");
            }
        }

       
    }

  
}
