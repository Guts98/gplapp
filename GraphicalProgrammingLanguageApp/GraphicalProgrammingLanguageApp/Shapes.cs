﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Shapes Interface which can be implemented by shape classes.
    /// </summary>
    interface Shapes
    {
        /// <summary>
        /// Sets the basic properties of a shape.
        /// </summary>
        /// <param name="c">Outline color of the shape.</param>
        /// <param name="list">Parameters for defining the shape.</param>
        void set(Color c, params int[] list);
        
        /// <summary>
        /// Draws the shape on the given graphics.
        /// </summary>
        /// <param name="g">The Graphics on which the shape is to be drawn.</param>
        void draw(Graphics g);

        /// <summary>
        /// Calculates the surface area of the shape.
        /// </summary>
        /// <returns>Calculated area of the shape.</returns>
        double calcArea();

        /// <summary>
        /// Calculates the perimeter of the shape.
        /// </summary>
        /// <returns>Calculated Parameter of the shape.</returns>
        double calcPerimeter();
    }
}
