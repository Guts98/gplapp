﻿namespace GraphicalProgrammingLanguageApp
{
    partial class GPLForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GPLForm));
            this.outputScreen = new System.Windows.Forms.PictureBox();
            this.runBtn = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.loadMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.syntaxGlossaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.syntaxBtn = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.separatorPanel = new System.Windows.Forms.Panel();
            this.errorHeaderPanel = new System.Windows.Forms.Panel();
            this.cursorPositionInfo = new System.Windows.Forms.Label();
            this.errorHeaderLabel = new System.Windows.Forms.Label();
            this.penColorIndicator = new System.Windows.Forms.Label();
            this.brushColorIndicator = new System.Windows.Forms.Label();
            this.penImgLabel = new System.Windows.Forms.Button();
            this.brushImgLabel = new System.Windows.Forms.Button();
            this.commandLine = new System.Windows.Forms.RichTextBox();
            this.multiCommandLine = new System.Windows.Forms.RichTextBox();
            this.rightBorderPanel = new System.Windows.Forms.Panel();
            this.errorDisplay = new System.Windows.Forms.RichTextBox();
            this.dimensionLabel = new System.Windows.Forms.Label();
            this.fileNameLabel = new System.Windows.Forms.Label();
            this.fileNamePanel = new System.Windows.Forms.Panel();
            this.fileCloseBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.outputScreen)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.errorHeaderPanel.SuspendLayout();
            this.fileNamePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // outputScreen
            // 
            this.outputScreen.BackColor = System.Drawing.Color.WhiteSmoke;
            this.outputScreen.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.outputScreen.Location = new System.Drawing.Point(477, 41);
            this.outputScreen.Name = "outputScreen";
            this.outputScreen.Size = new System.Drawing.Size(991, 465);
            this.outputScreen.TabIndex = 2;
            this.outputScreen.TabStop = false;
            this.outputScreen.Paint += new System.Windows.Forms.PaintEventHandler(this.outputScreen_Paint);
            // 
            // runBtn
            // 
            this.runBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(115)))), ((int)(((byte)(21)))));
            this.runBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.runBtn.FlatAppearance.BorderSize = 0;
            this.runBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.runBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(121)))), ((int)(((byte)(231)))));
            this.runBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.runBtn.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.runBtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.runBtn.Location = new System.Drawing.Point(5, 701);
            this.runBtn.Name = "runBtn";
            this.runBtn.Size = new System.Drawing.Size(205, 58);
            this.runBtn.TabIndex = 3;
            this.runBtn.Text = "Run";
            this.runBtn.UseVisualStyleBackColor = false;
            this.runBtn.Click += new System.EventHandler(this.runBtn_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.menuStrip1.Font = new System.Drawing.Font("Montserrat", 12F);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.helpMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 5, 0, 5);
            this.menuStrip1.Size = new System.Drawing.Size(1490, 41);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadMenuItem,
            this.saveMenuItem,
            this.saveAsImageToolStripMenuItem,
            this.exitMenu});
            this.fileMenu.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.fileMenu.Image = ((System.Drawing.Image)(resources.GetObject("fileMenu.Image")));
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(81, 31);
            this.fileMenu.Text = "File";
            this.fileMenu.MouseEnter += new System.EventHandler(this.fileMenu_MouseEnter);
            this.fileMenu.MouseLeave += new System.EventHandler(this.fileMenu_MouseLeave);
            // 
            // loadMenuItem
            // 
            this.loadMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.loadMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.loadMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("loadMenuItem.Image")));
            this.loadMenuItem.Name = "loadMenuItem";
            this.loadMenuItem.Size = new System.Drawing.Size(242, 32);
            this.loadMenuItem.Text = "Load";
            this.loadMenuItem.Click += new System.EventHandler(this.loadMenuItem_Click);
            // 
            // saveMenuItem
            // 
            this.saveMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.saveMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.saveMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveMenuItem.Image")));
            this.saveMenuItem.Name = "saveMenuItem";
            this.saveMenuItem.Size = new System.Drawing.Size(242, 32);
            this.saveMenuItem.Text = "Save";
            this.saveMenuItem.Click += new System.EventHandler(this.saveMenuItem_Click);
            // 
            // saveAsImageToolStripMenuItem
            // 
            this.saveAsImageToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.saveAsImageToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.saveAsImageToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveAsImageToolStripMenuItem.Image")));
            this.saveAsImageToolStripMenuItem.Name = "saveAsImageToolStripMenuItem";
            this.saveAsImageToolStripMenuItem.Size = new System.Drawing.Size(242, 32);
            this.saveAsImageToolStripMenuItem.Text = "Save as Image";
            this.saveAsImageToolStripMenuItem.Click += new System.EventHandler(this.saveAsImageToolStripMenuItem_Click);
            // 
            // exitMenu
            // 
            this.exitMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.exitMenu.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.exitMenu.Image = ((System.Drawing.Image)(resources.GetObject("exitMenu.Image")));
            this.exitMenu.Name = "exitMenu";
            this.exitMenu.Size = new System.Drawing.Size(242, 32);
            this.exitMenu.Text = "Exit";
            this.exitMenu.Click += new System.EventHandler(this.exitMenu_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.syntaxGlossaryToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpMenu.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.helpMenu.Image = ((System.Drawing.Image)(resources.GetObject("helpMenu.Image")));
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(93, 31);
            this.helpMenu.Text = "Help";
            this.helpMenu.MouseEnter += new System.EventHandler(this.helpMenu_MouseEnter);
            this.helpMenu.MouseLeave += new System.EventHandler(this.helpMenu_MouseLeave);
            // 
            // syntaxGlossaryToolStripMenuItem
            // 
            this.syntaxGlossaryToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.syntaxGlossaryToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.syntaxGlossaryToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("syntaxGlossaryToolStripMenuItem.Image")));
            this.syntaxGlossaryToolStripMenuItem.Name = "syntaxGlossaryToolStripMenuItem";
            this.syntaxGlossaryToolStripMenuItem.Size = new System.Drawing.Size(255, 32);
            this.syntaxGlossaryToolStripMenuItem.Text = "Syntax Glossary";
            this.syntaxGlossaryToolStripMenuItem.Click += new System.EventHandler(this.syntaxGlossaryToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.aboutToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.aboutToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.aboutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("aboutToolStripMenuItem.Image")));
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(255, 32);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // syntaxBtn
            // 
            this.syntaxBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(115)))), ((int)(((byte)(21)))));
            this.syntaxBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.syntaxBtn.FlatAppearance.BorderSize = 0;
            this.syntaxBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.syntaxBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(121)))), ((int)(((byte)(231)))));
            this.syntaxBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.syntaxBtn.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.syntaxBtn.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.syntaxBtn.Location = new System.Drawing.Point(216, 701);
            this.syntaxBtn.Name = "syntaxBtn";
            this.syntaxBtn.Size = new System.Drawing.Size(239, 58);
            this.syntaxBtn.TabIndex = 3;
            this.syntaxBtn.Text = "Syntax Check";
            this.syntaxBtn.UseVisualStyleBackColor = false;
            this.syntaxBtn.Click += new System.EventHandler(this.syntaxBtn_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 41);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 723);
            this.splitter1.TabIndex = 6;
            this.splitter1.TabStop = false;
            // 
            // separatorPanel
            // 
            this.separatorPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.separatorPanel.Location = new System.Drawing.Point(456, 41);
            this.separatorPanel.Name = "separatorPanel";
            this.separatorPanel.Size = new System.Drawing.Size(24, 718);
            this.separatorPanel.TabIndex = 7;
            // 
            // errorHeaderPanel
            // 
            this.errorHeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.errorHeaderPanel.Controls.Add(this.cursorPositionInfo);
            this.errorHeaderPanel.Controls.Add(this.errorHeaderLabel);
            this.errorHeaderPanel.Location = new System.Drawing.Point(477, 504);
            this.errorHeaderPanel.Name = "errorHeaderPanel";
            this.errorHeaderPanel.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.errorHeaderPanel.Size = new System.Drawing.Size(1001, 40);
            this.errorHeaderPanel.TabIndex = 0;
            // 
            // cursorPositionInfo
            // 
            this.cursorPositionInfo.AutoSize = true;
            this.cursorPositionInfo.Font = new System.Drawing.Font("Montserrat", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cursorPositionInfo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cursorPositionInfo.Location = new System.Drawing.Point(884, 5);
            this.cursorPositionInfo.Name = "cursorPositionInfo";
            this.cursorPositionInfo.Size = new System.Drawing.Size(59, 21);
            this.cursorPositionInfo.TabIndex = 2;
            this.cursorPositionInfo.Text = "x:0, y:0";
            // 
            // errorHeaderLabel
            // 
            this.errorHeaderLabel.AutoSize = true;
            this.errorHeaderLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.errorHeaderLabel.Font = new System.Drawing.Font("Montserrat", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorHeaderLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(222)))), ((int)(((byte)(111)))));
            this.errorHeaderLabel.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.errorHeaderLabel.Location = new System.Drawing.Point(3, 2);
            this.errorHeaderLabel.Name = "errorHeaderLabel";
            this.errorHeaderLabel.Padding = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.errorHeaderLabel.Size = new System.Drawing.Size(100, 34);
            this.errorHeaderLabel.TabIndex = 1;
            this.errorHeaderLabel.Text = "0 Errors";
            // 
            // penColorIndicator
            // 
            this.penColorIndicator.AutoSize = true;
            this.penColorIndicator.BackColor = System.Drawing.Color.WhiteSmoke;
            this.penColorIndicator.Font = new System.Drawing.Font("Montserrat", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.penColorIndicator.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.penColorIndicator.Location = new System.Drawing.Point(1430, 455);
            this.penColorIndicator.Name = "penColorIndicator";
            this.penColorIndicator.Size = new System.Drawing.Size(18, 20);
            this.penColorIndicator.TabIndex = 15;
            this.penColorIndicator.Text = "3";
            // 
            // brushColorIndicator
            // 
            this.brushColorIndicator.AutoSize = true;
            this.brushColorIndicator.BackColor = System.Drawing.Color.WhiteSmoke;
            this.brushColorIndicator.Font = new System.Drawing.Font("Montserrat", 7.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brushColorIndicator.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.brushColorIndicator.Location = new System.Drawing.Point(1430, 480);
            this.brushColorIndicator.Name = "brushColorIndicator";
            this.brushColorIndicator.Size = new System.Drawing.Size(31, 20);
            this.brushColorIndicator.TabIndex = 15;
            this.brushColorIndicator.Text = "off";
            // 
            // penImgLabel
            // 
            this.penImgLabel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.penImgLabel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("penImgLabel.BackgroundImage")));
            this.penImgLabel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.penImgLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.penImgLabel.FlatAppearance.BorderSize = 0;
            this.penImgLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.penImgLabel.Font = new System.Drawing.Font("Montserrat", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.penImgLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.penImgLabel.Location = new System.Drawing.Point(1388, 451);
            this.penImgLabel.Name = "penImgLabel";
            this.penImgLabel.Size = new System.Drawing.Size(36, 24);
            this.penImgLabel.TabIndex = 14;
            this.penImgLabel.UseVisualStyleBackColor = false;
            // 
            // brushImgLabel
            // 
            this.brushImgLabel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.brushImgLabel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("brushImgLabel.BackgroundImage")));
            this.brushImgLabel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.brushImgLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.brushImgLabel.FlatAppearance.BorderSize = 0;
            this.brushImgLabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.brushImgLabel.Font = new System.Drawing.Font("Montserrat", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.brushImgLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.brushImgLabel.Location = new System.Drawing.Point(1388, 476);
            this.brushImgLabel.Name = "brushImgLabel";
            this.brushImgLabel.Size = new System.Drawing.Size(36, 24);
            this.brushImgLabel.TabIndex = 14;
            this.brushImgLabel.UseVisualStyleBackColor = false;
            this.brushImgLabel.Click += new System.EventHandler(this.fileCloseBtn_Click);
            // 
            // commandLine
            // 
            this.commandLine.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.commandLine.Font = new System.Drawing.Font("Montserrat", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commandLine.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(224)))), ((int)(((byte)(196)))));
            this.commandLine.Location = new System.Drawing.Point(0, 656);
            this.commandLine.Multiline = false;
            this.commandLine.Name = "commandLine";
            this.commandLine.Size = new System.Drawing.Size(455, 39);
            this.commandLine.TabIndex = 9;
            this.commandLine.Text = "";
            this.commandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.commandLine_KeyDown);
            // 
            // multiCommandLine
            // 
            this.multiCommandLine.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.multiCommandLine.Font = new System.Drawing.Font("Montserrat", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.multiCommandLine.ForeColor = System.Drawing.Color.Gold;
            this.multiCommandLine.Location = new System.Drawing.Point(0, 76);
            this.multiCommandLine.Name = "multiCommandLine";
            this.multiCommandLine.Size = new System.Drawing.Size(455, 588);
            this.multiCommandLine.TabIndex = 10;
            this.multiCommandLine.Text = "";
            this.multiCommandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.multiCommandLine_KeyDown);
            // 
            // rightBorderPanel
            // 
            this.rightBorderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.rightBorderPanel.Location = new System.Drawing.Point(1467, 35);
            this.rightBorderPanel.Name = "rightBorderPanel";
            this.rightBorderPanel.Size = new System.Drawing.Size(24, 724);
            this.rightBorderPanel.TabIndex = 7;
            // 
            // errorDisplay
            // 
            this.errorDisplay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(31)))), ((int)(((byte)(68)))));
            this.errorDisplay.Font = new System.Drawing.Font("Montserrat", 9F, System.Drawing.FontStyle.Bold);
            this.errorDisplay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(222)))), ((int)(((byte)(111)))));
            this.errorDisplay.Location = new System.Drawing.Point(477, 536);
            this.errorDisplay.Name = "errorDisplay";
            this.errorDisplay.ReadOnly = true;
            this.errorDisplay.Size = new System.Drawing.Size(988, 223);
            this.errorDisplay.TabIndex = 11;
            this.errorDisplay.Text = "No Errors Found";
            // 
            // dimensionLabel
            // 
            this.dimensionLabel.AutoSize = true;
            this.dimensionLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.dimensionLabel.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.dimensionLabel.Location = new System.Drawing.Point(1400, 18);
            this.dimensionLabel.Name = "dimensionLabel";
            this.dimensionLabel.Size = new System.Drawing.Size(67, 20);
            this.dimensionLabel.TabIndex = 12;
            this.dimensionLabel.Text = "985x455";
            // 
            // fileNameLabel
            // 
            this.fileNameLabel.AutoSize = true;
            this.fileNameLabel.BackColor = System.Drawing.Color.Transparent;
            this.fileNameLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.fileNameLabel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.fileNameLabel.Font = new System.Drawing.Font("Montserrat", 10F);
            this.fileNameLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.fileNameLabel.Location = new System.Drawing.Point(3, 4);
            this.fileNameLabel.Name = "fileNameLabel";
            this.fileNameLabel.Size = new System.Drawing.Size(143, 26);
            this.fileNameLabel.TabIndex = 13;
            this.fileNameLabel.Text = "Untitled.gpl.txt";
            // 
            // fileNamePanel
            // 
            this.fileNamePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(31)))), ((int)(((byte)(40)))));
            this.fileNamePanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.fileNamePanel.Controls.Add(this.fileCloseBtn);
            this.fileNamePanel.Controls.Add(this.fileNameLabel);
            this.fileNamePanel.Location = new System.Drawing.Point(0, 41);
            this.fileNamePanel.Name = "fileNamePanel";
            this.fileNamePanel.Size = new System.Drawing.Size(455, 32);
            this.fileNamePanel.TabIndex = 14;
            // 
            // fileCloseBtn
            // 
            this.fileCloseBtn.BackColor = System.Drawing.Color.Transparent;
            this.fileCloseBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("fileCloseBtn.BackgroundImage")));
            this.fileCloseBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fileCloseBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.fileCloseBtn.FlatAppearance.BorderSize = 0;
            this.fileCloseBtn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.fileCloseBtn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkRed;
            this.fileCloseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fileCloseBtn.Font = new System.Drawing.Font("Montserrat", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileCloseBtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.fileCloseBtn.Location = new System.Drawing.Point(420, 4);
            this.fileCloseBtn.Name = "fileCloseBtn";
            this.fileCloseBtn.Size = new System.Drawing.Size(28, 27);
            this.fileCloseBtn.TabIndex = 14;
            this.fileCloseBtn.UseVisualStyleBackColor = false;
            this.fileCloseBtn.Click += new System.EventHandler(this.fileCloseBtn_Click);
            // 
            // GPLForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(62)))), ((int)(((byte)(59)))));
            this.ClientSize = new System.Drawing.Size(1490, 764);
            this.Controls.Add(this.penImgLabel);
            this.Controls.Add(this.errorDisplay);
            this.Controls.Add(this.errorHeaderPanel);
            this.Controls.Add(this.brushColorIndicator);
            this.Controls.Add(this.penColorIndicator);
            this.Controls.Add(this.fileNamePanel);
            this.Controls.Add(this.dimensionLabel);
            this.Controls.Add(this.brushImgLabel);
            this.Controls.Add(this.multiCommandLine);
            this.Controls.Add(this.commandLine);
            this.Controls.Add(this.rightBorderPanel);
            this.Controls.Add(this.separatorPanel);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.syntaxBtn);
            this.Controls.Add(this.runBtn);
            this.Controls.Add(this.outputScreen);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Montserrat", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "GPLForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Graphical Programming Language App";
            this.Load += new System.EventHandler(this.GPLForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.outputScreen)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.errorHeaderPanel.ResumeLayout(false);
            this.errorHeaderPanel.PerformLayout();
            this.fileNamePanel.ResumeLayout(false);
            this.fileNamePanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox outputScreen;
        private System.Windows.Forms.Button runBtn;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem exitMenu;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripMenuItem syntaxGlossaryToolStripMenuItem;
        private System.Windows.Forms.Button syntaxBtn;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel separatorPanel;
        private System.Windows.Forms.Panel errorHeaderPanel;
        private System.Windows.Forms.Label errorHeaderLabel;
        private System.Windows.Forms.RichTextBox commandLine;
        private System.Windows.Forms.RichTextBox multiCommandLine;
        private System.Windows.Forms.Panel rightBorderPanel;
        private System.Windows.Forms.RichTextBox errorDisplay;
        private System.Windows.Forms.Label dimensionLabel;
        private System.Windows.Forms.ToolStripMenuItem loadMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveMenuItem;
        private System.Windows.Forms.Label fileNameLabel;
        private System.Windows.Forms.Panel fileNamePanel;
        private System.Windows.Forms.Button fileCloseBtn;
        private System.Windows.Forms.Button brushImgLabel;
        private System.Windows.Forms.Button penImgLabel;
        private System.Windows.Forms.Label penColorIndicator;
        private System.Windows.Forms.Label brushColorIndicator;
        private System.Windows.Forms.Label cursorPositionInfo;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsImageToolStripMenuItem;
    }
}

