﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Triangle class used to define and draw triangle shape
    /// </summary>
    /// <remarks>Inherits <see cref="abstract class Shape : Shapes"/></remarks>
    class Triangle : Shape
    {
        /// <summary>
        /// base side of the triangle
        /// </summary>
        int baseSide;

        /// <summary>
        /// Height of the triangle
        /// </summary>
        int height;

        /// <summary>
        /// Default Constructor that defines a default triangle with size base 100 by height 100
        /// </summary>
        /// <remarks>Calls base constructor to define color, x and y </remarks>
        public Triangle() : base()
        {
            this.baseSide = 100;
            this.height = 100;
        }

        /// <summary>
        /// Parameterized constructor that provides the color, base side and height as well as cursor position of the triangle
        /// </summary>
        /// <param name="color">Outline color for the triangle</param>
        /// <param name="x">X position on the graphics</param>
        /// <param name="y">Y position on the graphics</param>
        /// <param name="baseSide">Base Side of the triangle</param>
        /// <param name="height">Height of the triangle</param>
        /// <remarks>Calls base constructor to define color, x and y </remarks>
        public Triangle(Color color, int x, int y, int baseSide, int height) : base(color, x, y)
        {
            this.baseSide = baseSide;                 //set the width of the triangle
            this.height = height;                     //sets the height of the triangle
        }

        /// <summary>
        /// Sets the color, cursor position as well as the baseSide and height of the triangle    
        /// </summary>
        /// <param name="color">Outline color for the triangle</param>
        /// <param name="list">Integer parameter list containing x position, y positoin and baseSide and height of the triangle</param>
        public override void set(Color color, params int[] list)
        {
            if ((list.Length) != 4)                                     //check and reports invalid number of parameters for this shape
                throw new InvalidParameterException("Wrong number of parameters '" + (list.Length - 2) + "' given, 2 expected for a triangle");

            base.set(color, list[0], list[1]);           //list[0] and list[1] being x and y coordinates 
            this.baseSide = list[2];
            this.height = list[3];
        }

        /// <summary>
        /// Draws triangle on the provided graphics 
        /// </summary>
        /// <param name="g">The graphics provided by the caller on which the triangle is to be drawn</param>
        /// <remarks>The triangle is drawn by deducing 3 vertices from the height and width provided and then drawing a polygon out of those vertices.</remarks>
        public override void draw(Graphics g)
        {
            Pen p = new Pen(base.PenColor, base.PenSize);             //uses the color and pen size defined in shape by default
            SolidBrush sb = new SolidBrush(base.FillColor);              //new solid brush to fill shapes wth the color defined in shape class by default

            //deriving starting position to bottom left vertex
            int x = base.x - this.baseSide / 2;
            int y = base.y + this.height / 2;
            //deriving all 3 vertices
            Point p1 = new Point(x, y);
            Point p2 = new Point(x + this.baseSide, y);
            Point p3 = new Point(base.x, base.y - this.height / 2);
            Point[] vertices = { p1, p2, p3 };

            //Draw Triangle polygon Shape on the provided graphics
            if (FillStatus)                             //if fill is on
            {
                g.DrawPolygon(p, vertices);         //draw hollow triangle
                g.FillPolygon(sb, vertices);        //draw filled triangle
            }
            else
                g.DrawPolygon(p, vertices);         //draw hollow triangle         
        }

        /// <summary>
        /// Calculates the surface area of the triangle.
        /// </summary>
        /// <returns>Calculated area of the triangle.</returns>
        public override double calcArea()
        {
            return ((baseSide * height) / 2);
        }

        /// <summary>
        /// Calculates the perimeter of the triangle.
        /// </summary>
        /// <returns>Calculated Parameter of the triangle.</returns>
        public override double calcPerimeter()
        {
            double s1,s2, s3 = this.baseSide;
            s1 = s2 =  ( baseSide + System.Math.Sqrt( System.Math.Pow(this.baseSide/2,2) + System.Math.Pow(this.height / 2, 2) ) );

            return (s1 + s2 + s3);
        }
    }
}
