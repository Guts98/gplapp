﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Commands interface which provides abstract methods for child implementation classes 
    /// </summary>
    interface Commands
    {
        /// <summary>
        /// Executes the command requested
        /// </summary>
        void Execute();

    }
}
