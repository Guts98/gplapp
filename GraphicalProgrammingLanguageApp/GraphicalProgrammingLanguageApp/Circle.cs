﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Circle class used to define and draw circle shape
    /// </summary>
    /// <remarks>Inherits <see cref="abstract class Shape : Shapes"/></remarks>
    class Circle : Shape
    {
        /// <summary>
        /// Radius of the circle
        /// </summary>
        int radius;

        /// <summary>
        /// Default Constructor that defines a default circle with radius 100 
        /// </summary>
        /// <remarks>Calls base constructor to define color, x and y </remarks>
        public Circle() : base()
        {
            this.radius = 100;
        }

        /// <summary>
        /// Parameterized constructor that provides the color,radius as well as cursor position of the circle
        /// </summary>
        /// <param name="color">Outline color for the circle</param>
        /// <param name="x">X position on the graphics</param>
        /// <param name="y">Y position on the graphics</param>
        /// <param name="radius">Radius of the circle</param>
        /// <remarks>Calls base constructor to define color, x and y </remarks>
        public Circle(Color color, int x, int y, int radius) : base(color, x, y)
        {
            this.radius = radius;                 //set the radius of the circle
        }

        /// <summary>
        /// Sets the color, cursor position as well as the radius of the circle    
        /// </summary>
        /// <param name="color">Outline color for the circle</param>
        /// <param name="list">Integer parameter list containing x position, y positoin and radius of the circle</param>
        public override void set(Color color, params int[] list)
        {
            if ((list.Length) != 3)                                 //check and reports invalid number of parameters for this shape
                throw new InvalidParameterException("Wrong number of parameters '" + (list.Length - 2) + "' given, 1 expected for circle");

            base.set(color, list[0], list[1]);           //list[0] and list[1] being x and y coordinates 
            this.radius = list[2];
        }

        /// <summary>
        /// Draws circle on the provided graphics 
        /// </summary>
        /// <param name="g">The graphics provided by the caller on which the circle is to be drawn</param>
        public override void draw(Graphics g)
        {
               Pen p = new Pen(base.PenColor, base.PenSize);             //uses the color and pen size defined in shape by default
               SolidBrush sb = new SolidBrush(base.FillColor);              //new solid brush to fill shapes wth the color defined in shape class by default

               if(FillStatus)
               {
                   //Draw Ellipse Shape on the provided graphics
                   g.FillEllipse(sb, (x - radius), (y - radius), (radius * 2), (radius * 2));
                   g.DrawEllipse(p, (x - radius), (y - radius), (radius * 2), (radius * 2));
               }
               else
                   g.DrawEllipse(p, (x - radius), (y - radius), (radius * 2), (radius * 2));

        }




        /// <summary>
        /// Calculates the surface area of the circle.
        /// </summary>
        /// <returns>Calculated area of the circle.</returns>
        public override double calcArea()
        {
            return (3.14 * this.radius * this.radius);
        }

        /// <summary>
        /// Calculates the perimeter of the circle.
        /// </summary>
        /// <returns>Calculated Parameter of the circle.</returns>
        public override double calcPerimeter()
        {
            return (2 * 3.14 * this.radius);
        }
    }
}
