﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// SyntaxColorRTB class that inherits <see cref="System.Windows.Forms.RichTextBox"/> class and
    /// contains methods which triggers syntax coloring when a key is pressed in the textbox
    /// </summary>
    class SyntaxColorRTB : System.Windows.Forms.RichTextBox
    {
        /// <summary>
        /// Imports required dll files to support the text box enabling and disabling 
        /// calls for locking window
        /// </summary>
        /// <param name="hWnd"></param>
        /// <returns></returns>
        [DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int LockWindowUpdate(int hWnd);

        /// <summary>
        /// Overrides the base's ontextchanged method and applying the syntax coloring
        /// </summary>
        /// <param name="e"></param>
        protected override void OnTextChanged(System.EventArgs e)
        {
            base.OnTextChanged(e);
            ColorTheKeyWords();
        }
       
        /// <summary>
        /// Searches for given keywords and selects and colors them
        /// </summary>
        private void ColorTheKeyWords()
        {   
            int SelectionAt = this.SelectionStart;
            LockWindowUpdate(this.Handle.ToInt32());
            
            foreach(String line in this.Lines)
            {
                changeKeyWordColor(line, SelectionAt);
            }
            LockWindowUpdate(0);
        }

        /// <summary>
        /// Changes te keyword color of given line from specified selection index
        /// </summary>
        /// <param name="line">line to be colored</param>
        /// <param name="SelectionAt"></param>
        public void changeKeyWordColor(String line, int SelectionAt)
        {
            //Match m = Regex.Match(this.Text, @"(\bdrawto\b|\bmoveto|\bsquare|\brect|\bcircle\b)", RegexOptions.IgnoreCase);
          //  String errorKeyWords = @"(\bInvalidCommandException\b|\InvalidColorException|\bsquare|\brect|\bcircle\b)";
            Match m = Regex.Match(line, @"\b(InvalidCommandException|InvalidColorException)\b", RegexOptions.IgnoreCase);
            if (m.Success)
            {
                this.SelectionStart = m.Index;
                this.SelectionLength = m.Length;
                this.SelectionColor = Color.Orange;
            }
         //   this.SelectionStart = this.SelectionStart; // SelectionAt;
         //   this.SelectionLength = 0;
          //  this.SelectionColor = System.Drawing.Color.Gold;
        }
    }
}
