﻿namespace GraphicalProgrammingLanguageApp
{
    partial class SyntaxHelpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.supportedColorHeaderLabel = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gplSyntaxesDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gplSyntaxesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gplSyntaxDBDataSet = new GraphicalProgrammingLanguageApp.gplSyntaxDBDataSet();
            this.authorInfoLabel = new System.Windows.Forms.Label();
            this.validPenSizeLabel = new System.Windows.Forms.Label();
            this.fillValuesLabel = new System.Windows.Forms.Label();
            this.hexColorInfoLabel = new System.Windows.Forms.Label();
            this.penSizeInfoLabel = new System.Windows.Forms.Label();
            this.supportedColorListLabel = new System.Windows.Forms.Label();
            this.fillOptionsLabel = new System.Windows.Forms.Label();
            this.gplSyntaxesTableAdapter = new GraphicalProgrammingLanguageApp.gplSyntaxDBDataSetTableAdapters.gplSyntaxesTableAdapter();
            this.tableAdapterManager = new GraphicalProgrammingLanguageApp.gplSyntaxDBDataSetTableAdapters.TableAdapterManager();
            this.gplSyntaxDBDataSet1 = new GraphicalProgrammingLanguageApp.gplSyntaxDBDataSet();
            this.gplSyntaxesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxDBDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxesBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Montserrat", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(198)))), ((int)(((byte)(35)))));
            this.label1.Location = new System.Drawing.Point(251, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(680, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "Graphical Programming Language: Syntax Guide";
            this.label1.UseMnemonic = false;
            // 
            // supportedColorHeaderLabel
            // 
            this.supportedColorHeaderLabel.AutoSize = true;
            this.supportedColorHeaderLabel.Font = new System.Drawing.Font("Montserrat", 8.25F, System.Drawing.FontStyle.Bold);
            this.supportedColorHeaderLabel.ForeColor = System.Drawing.Color.SpringGreen;
            this.supportedColorHeaderLabel.Location = new System.Drawing.Point(324, 557);
            this.supportedColorHeaderLabel.Name = "supportedColorHeaderLabel";
            this.supportedColorHeaderLabel.Size = new System.Drawing.Size(152, 20);
            this.supportedColorHeaderLabel.TabIndex = 1;
            this.supportedColorHeaderLabel.Text = "Supported Colors:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InfoText;
            this.panel1.Controls.Add(this.gplSyntaxesDataGridView);
            this.panel1.Controls.Add(this.authorInfoLabel);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.validPenSizeLabel);
            this.panel1.Controls.Add(this.fillValuesLabel);
            this.panel1.Controls.Add(this.hexColorInfoLabel);
            this.panel1.Controls.Add(this.penSizeInfoLabel);
            this.panel1.Controls.Add(this.supportedColorListLabel);
            this.panel1.Controls.Add(this.fillOptionsLabel);
            this.panel1.Controls.Add(this.supportedColorHeaderLabel);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1241, 654);
            this.panel1.TabIndex = 2;
            // 
            // gplSyntaxesDataGridView
            // 
            this.gplSyntaxesDataGridView.AllowUserToAddRows = false;
            this.gplSyntaxesDataGridView.AllowUserToDeleteRows = false;
            this.gplSyntaxesDataGridView.AllowUserToOrderColumns = true;
            this.gplSyntaxesDataGridView.AutoGenerateColumns = false;
            this.gplSyntaxesDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Montserrat", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gplSyntaxesDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gplSyntaxesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gplSyntaxesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.gplSyntaxesDataGridView.DataSource = this.gplSyntaxesBindingSource;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Montserrat", 8F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gplSyntaxesDataGridView.DefaultCellStyle = dataGridViewCellStyle4;
            this.gplSyntaxesDataGridView.Location = new System.Drawing.Point(0, 54);
            this.gplSyntaxesDataGridView.Name = "gplSyntaxesDataGridView";
            this.gplSyntaxesDataGridView.ReadOnly = true;
            this.gplSyntaxesDataGridView.RowHeadersWidth = 51;
            this.gplSyntaxesDataGridView.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.gplSyntaxesDataGridView.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Montserrat", 8F);
            this.gplSyntaxesDataGridView.RowTemplate.DividerHeight = 2;
            this.gplSyntaxesDataGridView.RowTemplate.Height = 35;
            this.gplSyntaxesDataGridView.RowTemplate.ReadOnly = true;
            this.gplSyntaxesDataGridView.Size = new System.Drawing.Size(1238, 504);
            this.gplSyntaxesDataGridView.TabIndex = 7;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Id";
            this.dataGridViewTextBoxColumn1.FillWeight = 4F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Id";
            this.dataGridViewTextBoxColumn1.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Command";
            this.dataGridViewTextBoxColumn2.FillWeight = 9F;
            this.dataGridViewTextBoxColumn2.HeaderText = "Command";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ParameterLength";
            this.dataGridViewTextBoxColumn3.FillWeight = 14F;
            this.dataGridViewTextBoxColumn3.HeaderText = "ParameterLength";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Parameters";
            this.dataGridViewTextBoxColumn4.FillWeight = 27F;
            this.dataGridViewTextBoxColumn4.HeaderText = "Parameters";
            this.dataGridViewTextBoxColumn4.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Description";
            this.dataGridViewTextBoxColumn5.FillWeight = 46F;
            this.dataGridViewTextBoxColumn5.HeaderText = "Description";
            this.dataGridViewTextBoxColumn5.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // gplSyntaxesBindingSource
            // 
            this.gplSyntaxesBindingSource.DataMember = "gplSyntaxes";
            this.gplSyntaxesBindingSource.DataSource = this.gplSyntaxDBDataSet;
            // 
            // gplSyntaxDBDataSet
            // 
            this.gplSyntaxDBDataSet.DataSetName = "gplSyntaxDBDataSet";
            this.gplSyntaxDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // authorInfoLabel
            // 
            this.authorInfoLabel.AutoSize = true;
            this.authorInfoLabel.Font = new System.Drawing.Font("Montserrat", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.authorInfoLabel.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.authorInfoLabel.Location = new System.Drawing.Point(351, 617);
            this.authorInfoLabel.Name = "authorInfoLabel";
            this.authorInfoLabel.Size = new System.Drawing.Size(566, 20);
            this.authorInfoLabel.TabIndex = 6;
            this.authorInfoLabel.Text = "Copyright @ 2020 | Amit Kumar Karn | Kathmandu, Nepal | All Rights Reserved ";
            // 
            // validPenSizeLabel
            // 
            this.validPenSizeLabel.AutoSize = true;
            this.validPenSizeLabel.BackColor = System.Drawing.Color.Transparent;
            this.validPenSizeLabel.Font = new System.Drawing.Font("Montserrat", 8.25F);
            this.validPenSizeLabel.ForeColor = System.Drawing.Color.Snow;
            this.validPenSizeLabel.Location = new System.Drawing.Point(547, 577);
            this.validPenSizeLabel.Name = "validPenSizeLabel";
            this.validPenSizeLabel.Size = new System.Drawing.Size(250, 20);
            this.validPenSizeLabel.TabIndex = 1;
            this.validPenSizeLabel.Text = "Minimum Size 1, Maximum Size 20";
            // 
            // fillValuesLabel
            // 
            this.fillValuesLabel.AutoSize = true;
            this.fillValuesLabel.Font = new System.Drawing.Font("Montserrat", 8.25F);
            this.fillValuesLabel.ForeColor = System.Drawing.Color.Snow;
            this.fillValuesLabel.Location = new System.Drawing.Point(430, 597);
            this.fillValuesLabel.Name = "fillValuesLabel";
            this.fillValuesLabel.Size = new System.Drawing.Size(259, 20);
            this.fillValuesLabel.TabIndex = 1;
            this.fillValuesLabel.Text = "Only \'On\' or \'Off\' values are allowed.";
            // 
            // hexColorInfoLabel
            // 
            this.hexColorInfoLabel.AutoSize = true;
            this.hexColorInfoLabel.Font = new System.Drawing.Font("Montserrat", 8.25F);
            this.hexColorInfoLabel.ForeColor = System.Drawing.Color.Snow;
            this.hexColorInfoLabel.Location = new System.Drawing.Point(686, 597);
            this.hexColorInfoLabel.Name = "hexColorInfoLabel";
            this.hexColorInfoLabel.Size = new System.Drawing.Size(233, 20);
            this.hexColorInfoLabel.TabIndex = 1;
            this.hexColorInfoLabel.Text = "(Hex Color Codes are also valid!)";
            // 
            // penSizeInfoLabel
            // 
            this.penSizeInfoLabel.AutoSize = true;
            this.penSizeInfoLabel.Font = new System.Drawing.Font("Montserrat", 8.25F, System.Drawing.FontStyle.Bold);
            this.penSizeInfoLabel.ForeColor = System.Drawing.Color.SpringGreen;
            this.penSizeInfoLabel.Location = new System.Drawing.Point(385, 577);
            this.penSizeInfoLabel.Name = "penSizeInfoLabel";
            this.penSizeInfoLabel.Size = new System.Drawing.Size(156, 20);
            this.penSizeInfoLabel.TabIndex = 1;
            this.penSizeInfoLabel.Text = "Allowed Pen Sizes:";
            // 
            // supportedColorListLabel
            // 
            this.supportedColorListLabel.AutoSize = true;
            this.supportedColorListLabel.Font = new System.Drawing.Font("Montserrat", 7F);
            this.supportedColorListLabel.ForeColor = System.Drawing.Color.Snow;
            this.supportedColorListLabel.Location = new System.Drawing.Point(479, 561);
            this.supportedColorListLabel.Name = "supportedColorListLabel";
            this.supportedColorListLabel.Size = new System.Drawing.Size(540, 16);
            this.supportedColorListLabel.TabIndex = 1;
            this.supportedColorListLabel.Text = "RED, BLUE, GREEN, BLACK, WHITE, ORANGE, PINK, PURPLE, YELLOW and HEX CODES";
            // 
            // fillOptionsLabel
            // 
            this.fillOptionsLabel.AutoSize = true;
            this.fillOptionsLabel.Font = new System.Drawing.Font("Montserrat", 8.25F, System.Drawing.FontStyle.Bold);
            this.fillOptionsLabel.ForeColor = System.Drawing.Color.SpringGreen;
            this.fillOptionsLabel.Location = new System.Drawing.Point(322, 597);
            this.fillOptionsLabel.Name = "fillOptionsLabel";
            this.fillOptionsLabel.Size = new System.Drawing.Size(106, 20);
            this.fillOptionsLabel.TabIndex = 1;
            this.fillOptionsLabel.Text = "Fill Options:";
            // 
            // gplSyntaxesTableAdapter
            // 
            this.gplSyntaxesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.gplSyntaxesTableAdapter = this.gplSyntaxesTableAdapter;
            this.tableAdapterManager.UpdateOrder = GraphicalProgrammingLanguageApp.gplSyntaxDBDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // gplSyntaxDBDataSet1
            // 
            this.gplSyntaxDBDataSet1.DataSetName = "gplSyntaxDBDataSet";
            this.gplSyntaxDBDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gplSyntaxesBindingSource1
            // 
            this.gplSyntaxesBindingSource1.DataMember = "gplSyntaxes";
            this.gplSyntaxesBindingSource1.DataSource = this.gplSyntaxDBDataSet1;
            // 
            // SyntaxHelpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(140)))), ((int)(((byte)(207)))));
            this.ClientSize = new System.Drawing.Size(1241, 646);
            this.Controls.Add(this.panel1);
            this.Name = "SyntaxHelpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "GPL App: Syntax Help";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxDBDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gplSyntaxesBindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label supportedColorHeaderLabel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label supportedColorListLabel;
        private System.Windows.Forms.Label hexColorInfoLabel;
        private System.Windows.Forms.Label fillValuesLabel;
        private System.Windows.Forms.Label fillOptionsLabel;
        private gplSyntaxDBDataSet gplSyntaxDBDataSet;
        private System.Windows.Forms.BindingSource gplSyntaxesBindingSource;
        private gplSyntaxDBDataSetTableAdapters.gplSyntaxesTableAdapter gplSyntaxesTableAdapter;
        private gplSyntaxDBDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Label authorInfoLabel;
        private gplSyntaxDBDataSet gplSyntaxDBDataSet1;
        private System.Windows.Forms.BindingSource gplSyntaxesBindingSource1;
        private System.Windows.Forms.DataGridView gplSyntaxesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.Label validPenSizeLabel;
        private System.Windows.Forms.Label penSizeInfoLabel;
    }
}