﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Command abstract class
    /// TO BE IMPLEMENTED
    /// </summary>
    public abstract class Command:Commands
    {
        /// <summary>
        /// Stores all the token or keywords with a reference name as their key
        /// </summary>
        public Hashtable tokens;

        /// <summary>
        /// Used to store the words of a sentence in array after spliting by some character
        /// </summary>
        public String[] splitWords;

        /// <summary>
        /// Default constructor of Command class
        /// </summary>
        public Command()
        {
            this.tokens = new Hashtable();      //initialize tokens hashtable and the actual tokens will be added in child classes
        }

        /// <summary>
        /// Takes the reference of given tokens hashtable object and stores the reference in the instance ref variable tokens
        /// </summary>
        /// <param name="tokens">the refenrece of the tokens object</param>
        public void setCommands(Hashtable tokens)
        {
            this.tokens = tokens;

        }

        /// <summary>
        /// If the command hash table contains the given value
        /// </summary>
        /// <param name="cmd">The value that needs to be searched in the hashtable</param>
        /// <returns></returns>
        public bool contains(String cmd)
        {
           if (tokens.ContainsValue(cmd))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Executes the command requested
        /// </summary>
        public abstract void Execute();

        /// <summary>
        /// Parses the given command to deduce meaningful commands out of it
        /// </summary>
        /// <param name="objectList"></param>
        public abstract void parseCommand(params object[] objectList);
  
    }
}
