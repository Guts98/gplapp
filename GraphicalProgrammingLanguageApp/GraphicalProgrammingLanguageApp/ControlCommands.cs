﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// ControlCommands class that inherits <see cref="Command"/> class and provides commands
    /// for conditional operations in the program
    /// </summary>
    class ControlCommands:Command
    {
        /// <summary>
        /// Default constructor that adds conditional commands to the tokens object  
        /// </summary>
        public ControlCommands()
        {
            base.tokens.Add("cmdIF", "if");
            base.tokens.Add("cmdENDIF", "endif");
          //  base.tokens.Add("cmdELSE", "else");
            base.tokens.Add("cmdWHILE", "while");
            base.tokens.Add("cmdENDWHILE", "endwhile");
            base.tokens.Add("cmdMETHOD", "method");
            base.tokens.Add("cmdENDMETHOD", "endmethod");
           // base.tokens.Add("cmdVAR", "var");
        }

        /// <summary>
        /// Executes the command requested
        /// </summary>
        public override void Execute()
        {
            throw new NotImplementedException();
        }


        /// <summary>
        /// Parses the given command to deduce meaningful commands out of it
        /// </summary>
        /// <param name="objectList"></param>
        public override void parseCommand(params object[] objectList)
        {
            
            String line = (String)objectList[0];
            String[] splitLine = line.Split(' ');

        }
    }
}
