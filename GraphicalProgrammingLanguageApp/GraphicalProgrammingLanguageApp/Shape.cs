﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Shape class which can be inherited to define shape classes.
    /// </summary>
    /// <remarks>Implements <see cref=" interface Shapes"/></remarks>
    abstract class Shape : Shapes
    {
        /// <summary>
        /// x-coordinate of the canvas
        /// </summary>
        protected int x;

        /// <summary>
        /// y-coordinate of the canvas
        /// </summary>
        protected int y;

        
        /// <summary>
        /// Pen's color or border color for the shape
        /// </summary>
        protected Color color; 

        /// <summary>
        /// Width of the pen that draws shapes
        /// </summary>
        private float penSize;

        /// <summary>
        /// Color of the pen that draws shapes
        /// </summary>
        private Color penColor;

        /// <summary>
        /// Determines whether to fill the shape or not
        /// </summary>
        private bool fillStatus;

        /// <summary>
        /// Fill color of the shapes
        /// </summary>
        private Color fillColor;

        /// <summary>
        /// Sets and returns the value of <seealso cref="float penSize"/>
        /// </summary>
        public float PenSize { get => penSize; set => penSize = value; }

        /// <summary>
        /// Sets and returns the value of <seealso cref="bool penFillStatus"/>
        /// </summary>
        public bool FillStatus { get => fillStatus; set => fillStatus = value; }

        /// <summary>
        /// Sets and returns the value of <seealso cref="String fillColor"/>
        /// </summary>
        public Color FillColor { get => fillColor; set => fillColor = value; }

        /// <summary>
        /// Sets and returns the value of <seealso cref="String penColor"/>
        /// </summary>
        public Color PenColor { get => penColor; set => penColor = value; }


        /// <summary>
        /// Default constructor defining default pen color, fill color and initial position of cursor as well as the fill status
        /// </summary>
        public Shape()
        {
            this.x = this.y = 0;                            //sets initial position of the cursor to (0,0)
            this.color = Color.Black;                       //color set to black by default

            this.penSize = 2;                               //pen size set to 2 by default
            this.penColor = this.color;                    //pen color set to black by default
            this.fillStatus = false;                        //fill status set to off by default
            this.fillColor = Color.Green;                   //fill color set to green by deafult
        }

        /// <summary>
        /// Parameterized constructor that defines custom color and the cursor position
        /// </summary>
        /// <param name="color">Outline color of the shape</param>
        /// <param name="x">X-coordinate of the cursor</param>
        /// <param name="y">Y-coordinate of the cursor</param>
        public Shape(Color color, int x, int y)
        {
            this.penColor = this.color = color;             //set the outline color to the specified color
            this.x = x;                                     //set the x position of the cursor
            this.y = y;                                     //set the y-position of the cursor
        }

        /// <summary>
        /// Sets the basic properties of a shape i.e. outline color, x position and y position
        /// </summary>
        /// /// <param name="color">Outline color of the shape</param>
        /// <param name="list">Integer list of the parameters required to initialize the shape's properties</param>
        ///<remarks>Declared as virtual so it can be overridden by a more specific child version
        ///but is here so it can be called by that child version to do the generic stuff</remarks>
        public virtual void set(Color color, params int[] list)
        {
            this.penColor = this.color = color;                 //outline color of the shape set to specified color
            this.x = list[0];                                   //x positoin of the cursor set to specified value
            this.y = list[1];                                   //x positoin of the cursor set to specified value
        }

        /// <summary>
        /// Draws the shape on the given graphics
        /// </summary>
        /// <param name="g">Graphics object on which the shape is to be drawn</param>
        public abstract void draw(Graphics g);

        /// <summary>
        /// Calculates the surface area of the shape.
        /// </summary>
        /// <returns>Calculated area of the shape.</returns>
        public abstract double calcArea();

        /// <summary>
        /// Calculates the perimeter of the shape.
        /// </summary>
        /// <returns>Calculated Parameter of the shape.</returns>
        public abstract double calcPerimeter();

        /// <summary>
        /// Returns a string value for the object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString()+"   " + this.x + "," + this.y + " : ";
        }

    }
}
