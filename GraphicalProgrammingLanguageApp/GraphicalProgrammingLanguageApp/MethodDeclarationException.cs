﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// InvalidPenSizeException class that occurs when the size for pen is not provided in the range of 1 to 20 
    /// </summary>
    /// <remarks>Inherits <see cref="System.Exception"/></remarks>
    [Serializable]
    public class MethodDeclarationException : System.Exception
    {
        /// <summary>
        /// String message to be displayed when thrown
        /// </summary>
        public String message;

        /// <summary>
        /// Default constructor that sets the exception message to its base class
        /// </summary>
        public MethodDeclarationException() : base("Error in Defining new Method.") { }

        /// <summary>
        /// Parameterized constructor that takes one string message 
        /// </summary>
        /// <param name="message">string message</param>
        public MethodDeclarationException(String message) : base(message)
        {
            this.message = message;
        }


        /// <summary>
        /// Parameterized constructor that sets the exception message to its base class and adds a additional info to the message
        /// </summary>
        ///  <param name="message">Line Number at which the exception occured</param>
        public MethodDeclarationException(String message, int lineNo) : base(String.Format("MethodDeclarationException: "+ message +" at Line: {0}", lineNo)) { }
    }
}
