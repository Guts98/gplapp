﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Canvas class that defines the graphics on which all the shapes are to be drawn
    /// </summary>
    public class Canvas
    {
        /// <summary>
        /// Graphics reference of the bitmap's graphics to draw onto
        /// </summary>
        public Graphics graphics;

        /// <summary>
        /// Pen reference to pen object that draws on the bitmap's graphics
        /// </summary>
        public Pen pen;

        /// <summary>
        /// x-coordinate of the bitmap
        /// </summary>
        private int xPos;

        /// <summary>
        /// y-coordinate of the bitmap
        /// </summary>
        private int yPos;



        /// <summary>
        /// Sets and returns the value of x-coordinate
        /// </summary>
        public int XPos { get => xPos; set => xPos = value; }

        /// <summary>
        /// Sets and returns the value of y-coordinate
        /// </summary>
        public int YPos { get => yPos; set => yPos = value; }

        /// <summary>
        /// Keep track whether to draw a shape with filled color or hollow
        /// </summary>
        public bool fillStatus;

        /// <summary>
        /// Stores the color for filling the shapes
        /// </summary>
        public Color fillColor;

        /// <summary>
        /// Circle reference representing the pen color to be drawn on the canvas
        /// </summary>
        Circle penCircle;

        /// <summary>
        /// Circle reference representing the fill color to be drawn on the canvas
        /// </summary>
        Circle fillCircle;

        /// <summary>
        /// Default Constructor that uses graphics of a test bitmap object and sets default properties for drawing
        /// </summary>
        public Canvas()
        {
            Bitmap testBitmap = new Bitmap(985,455);
            this.graphics = Graphics.FromImage(testBitmap);                           //assigns the reference to bitmap's graphics object
            this.xPos = this.yPos = 0;                                               //initial cursor position set to 0,0 
            pen = new Pen(Color.Black, 2);                                           //pen object to draw on the bitmap graphics

            this.fillColor = Color.Green;                                           //default fill color
            this.fillStatus = false;                                                 //by default fill is turned off

            penCircle = new Circle();                                               //Circle object representing the pen color to be drawn on the canvas
            fillCircle = new Circle();                                              //Circle object representing the fill color to be drawn on the canvas
            updateColorIndicators();                                                //set and draw these representator circle on the canvas
        }

        /// <summary>
        /// Parameterized constructor taking graphics object of the bitmap to draw onto
        /// </summary>
        /// <param name="graphics">Graphics on which the drawing is to be done</param>
        public Canvas(Graphics graphics)
        {
            this.graphics = graphics;                                                //assigns the reference to bitmap's graphics object
            this.xPos = this.yPos = 0;                                               //initial cursor position set to 0,0 
            pen = new Pen(Color.Black, 2);                                           //pen object to draw on the bitmap graphics

            this.fillColor = Color.Green;                                           //default fill color
            this.fillStatus = false;                                                 //by default fill is turned off
            
            penCircle = new Circle();                                               //Circle object representing the pen color to be drawn on the canvas
            fillCircle = new Circle();                                              //Circle object representing the fill color to be drawn on the canvas
            updateColorIndicators();                                                //set and draw these representator circle on the canvas
        }

        /// <summary>
        /// Draws pen color and brush color indicators as circles on canvas
        /// </summary>
        public void updateColorIndicators()
        {
            penCircle.set((this.pen.Color), 890, 419, 7);                               //set pen circle indicator properties
            penCircle.FillColor = this.pen.Color;
            penCircle.PenColor = Color.Black;
            penCircle.PenSize = 1;

            fillCircle.set((this.fillColor), 890, 447, 7);                              //set fill circle indicator properties
            fillCircle.FillColor = this.fillColor;
            fillCircle.PenColor = Color.Black;
            fillCircle.PenSize = 1;
            penCircle.FillStatus = fillCircle.FillStatus = true;                        //turn on the fill property of circles

            penCircle.draw(graphics);                                                   //draw pen color indicator on the canvas
            fillCircle.draw(graphics);                                                  //draw fill color indicator on the canvas
        }

        /// <summary>
        /// Clears the bitmap's graphics of the picturebox
        /// </summary>
        public void clear()
        {
            graphics.Clear(Color.WhiteSmoke);                                            //clears the output bitmap's graphics and refills it with white color
            //reset the cursor pointer to initial position i.e. (0,0)
            this.xPos = 0;                                                          
            this.yPos = 0;
        }

        /// <summary>
        /// Resets pen and fill properties back to default
        /// </summary>
        public void reset()
        {
             this.xPos = this.yPos = 0;                                               //initial cursor position set to 0,0 
            pen = new Pen(Color.Black, 2);                                           //pen object to draw on the bitmap graphics
            this.fillColor = Color.Green;                                           //default fill color
            this.fillStatus = false;                                                 //by default fill is turned off
            ////reset the cursor pointer to initial position i.e. (0,0)
            //this.xPos = 0;
            //this.yPos = 0;
        }


        /// <summary>
        /// Sets the cursor pointer to the coordinates provided
        /// </summary>
        /// <param name="parameters">X and Y positions of the destanation</param>
        public void moveTo(int[] parameters)
        {
            if (parameters.Length != 2)                                      //check and reports invalid parameter length
            { throw new InvalidParameterException("Wrong number of parameters '" + parameters.Length + "' given, 2 expected for moveto command"); }

            //sets the coordinates to the provided values
            this.xPos = parameters[0];
            this.yPos = parameters[1];
        }

        /// <summary>
        /// Sets the pen color that draws on the canvas
        /// </summary>
        /// <param name="parameters"></param>
        public void setPenColor(String[] parameters)
        {
            if (parameters.Length != 1)                                      //check and reports invalid parameter length
            { throw new InvalidParameterException("Wrong number of parameters '" + parameters.Length + "' given, 1 expected for pencolor"); }

            String color = parameters[0].ToLower();
            if (color[0].Equals('#'))                                       //if hex value is recieved
            {  
                this.pen.Color = GetSolidColorBrush(color); 
                return; 
            }

            //check if the color matches the limited list of color list
            if (!(color.Equals("red") || color.Equals("blue") || color.Equals("green") || color.Equals("black") || color.Equals("white") || color.Equals("orange") ||
            color.Equals("pink") || color.Equals("purple") || color.Equals("yellow")))
            { throw new InvalidColorException(color); }

            String pColor = char.ToUpper(color[0]) + color.Substring(1);    //convert first letter capital
            this.pen.Color = Color.FromName(pColor);
        }


        /// <summary>
        /// Sets pen size to passed size
        /// </summary>
        /// <param name="parameters">Pen size to be set to canvas pen</param>
        public void setPenSize(int[] parameters)
        {
            if (parameters.Length != 1)                                     //check and reports invalid parameter length
            { throw new InvalidParameterException("Wrong number of parameters '" + parameters.Length + "' given, 1 expected for pensize"); }
            
            int pSize = parameters[0];
            if (pSize < 0 || pSize > 20)                                     //checks if the pen size is within a valid range and throws an exception if not
                throw new InvalidPenSizeException(pSize.ToString());
            this.pen.Width = pSize;
        }

        /// <summary>
        /// Sets fill color to passed color
        /// </summary>
        /// <param name="parameters">Pen color to be set to canvas pen</param>
        public void setFillColor(String[] parameters)
        {
            if (parameters.Length != 1)                                 //check and reports invalid parameter length
            { throw new InvalidParameterException("Wrong number of parameters '" + parameters.Length + "' given, 1 expected for fillcolor"); }

            String color = parameters[0].ToLower();
            if(color[0].Equals('#'))                                    //if hex value is recieved
            {
                this.fillColor  = GetSolidColorBrush(color);
                return;
            }

            //check if the color matches the limited list of color list
            if (!(color.Equals("red") || color.Equals("blue") || color.Equals("green") || color.Equals("black") || color.Equals("white") || color.Equals("orange") ||
                color.Equals("pink") || color.Equals("purple") || color.Equals("yellow")))
            {
                throw new InvalidColorException(color);
            }

            String pColor = char.ToUpper(color[0]) + color.Substring(1);        //convert first letter capital
            this.fillColor = Color.FromName(pColor);
        }

        /// <summary>
        /// Turns the fill on off off for the shapes
        /// </summary>
        /// <param name="parameters">'on' or 'off' values for fillStatus</param>
        public void setFillStatus(String[] parameters)
        {
            if (parameters.Length != 1)                                 //check and reports invalid parameter length
            { throw new InvalidParameterException("Wrong number of parameters '" + parameters.Length + "' given, 1 expected for fill status"); }

            String fStatus = parameters[0];
            if (fStatus.ToLower() == "on")
                this.fillStatus = true;
            else if (fStatus.ToLower() == "off")
                this.fillStatus = false;
            else                                                                    //catches and reports invalid parameter exception
                throw new InvalidParameterException("Format exception for fill status '" + fStatus + "' given, 'on' or 'off' expected");
        }


        /// <summary>
        /// Converts hex coded color to rgb
        /// </summary>
        /// <param name="hex">Hexadecimal coded color</param>
        /// <returns></returns>
        public Color GetSolidColorBrush(string hex)
        {
            Color color;
            hex = hex.Replace("#", string.Empty);
            byte[] argb = new byte[4];
            try
            {
                argb[0] = (byte)(Convert.ToUInt32(hex.Substring(0, 2), 16));
                argb[1] = (byte)(Convert.ToUInt32(hex.Substring(2, 2), 16));
                argb[2] = (byte)(Convert.ToUInt32(hex.Substring(4, 2), 16));
                argb[3] = 1;//(byte)(Convert.ToUInt32(hex.Substring(6, 2), 16));
                color = System.Drawing.Color.FromArgb(((int)(argb[0])), ((int)(argb[1])), ((int)((argb[2]))));
            }
            catch(System.Exception)                             //catches any exception and reports it as invalid color string execption
            {
                throw new InvalidColorException(hex);
            }
            return color;
        }
    }
}
