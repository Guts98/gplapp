﻿using System;
using System.Collections;
using GraphicalProgrammingLanguageApp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GPLUnitTest
{
    /// <summary>
    /// METHODCommandTest class that tests the if method declaration and method calls are being handled 
    /// properly by the parser logics
    /// </summary>
    [TestClass]
    public class METHODCommandTest
    {
        /// <summary>
        /// Tests the if method declaration are being handled 
        /// properly by the parser logics whether it stores the statements inside the
        /// method in the method object
        /// </summary>
        [TestMethod]
        public void Test_MethodDeclaration()
        {
            // ARRANGE
            CommandParser cmdParser = new CommandParser();      //Command Parser object to access its methods
            ProgramQueue pQueue = new ProgramQueue();           //ProgramQueue object to store the lines verified by commandParser method
            Hashtable variables = new Hashtable();              //Stores the variables
            ArrayList lines = new ArrayList();              //Stores the block of lines to be tester
            Method testMethodObject;
            lines.Add("method testMethod(param1, param2, param3)");
            lines.Add("drawto param1,param2");
            lines.Add("square param3");
            lines.Add("endmethod");

            // ACT
            int lineNo = 1;
            foreach (String line in lines)
            {   //calls the command parser method that analyzes the method body declaration command
                cmdParser.parseCommand(pQueue, line, variables, lineNo++);

            }
            testMethodObject = (Method)cmdParser.StoredMethods["testmethod"];
            // ASSERT                                           
            String expectedAddedCommand1 = "drawto param1,param2";
            String expectedAddedCommand2 = "square param3";
            String actualAddedCommands1 = ((ArrayList)testMethodObject.Statements)[0].ToString();
            String actualAddedCommands2 = ((ArrayList)testMethodObject.Statements)[1].ToString();
            int expectedNumberOfStatementsAdded = 2;
            int actualNumberOfStatementsAdded = ((ArrayList)testMethodObject.Statements).Count;
            int expectedNumOfParameters = 3;
            int actualNumOfParameters = testMethodObject.MethodParameters.Length;
            String expectedParam1 = "param1";
            String expectedParam2 = "param2";
            String expectedParam3 = "param3";
            String actualParam1 = testMethodObject.MethodParameters[0];
            String actualParam2 = testMethodObject.MethodParameters[1];
            String actualParam3= testMethodObject.MethodParameters[2];

            Assert.AreEqual(expectedAddedCommand1, actualAddedCommands1, "Statements were not added to the method block properly.");
            Assert.AreEqual(expectedAddedCommand2, actualAddedCommands2, "Statements were not added to the method block properly.");
            Assert.AreEqual(expectedNumberOfStatementsAdded, actualNumberOfStatementsAdded, 0.01, "Method did not add expected number of statements.");
            Assert.AreEqual(expectedParam1, actualParam1, "Parameters in method declaration did not match.");
            Assert.AreEqual(expectedParam2, actualParam2, "Parameters in method declaration did not match.");
            Assert.AreEqual(expectedParam3, actualParam3, "Parameters in method declaration did not match.");
            Assert.AreEqual(expectedNumOfParameters, actualNumOfParameters, 0.01, "Method did not add expected number of parameters.");
        }


        /// <summary>
        /// Tests the if method calls are being handled 
        /// properly by the parser logics whether it calls the right method object
        /// stored in the storedmethod hashtable of the command parser class
        /// </summary>
        [TestMethod]
        public void Test_MethodCall()
        {
            // ARRANGE
            CommandParser cmdParser = new CommandParser();      //Command Parser object to access its methods
            ProgramQueue pQueue = new ProgramQueue();           //ProgramQueue object to store the lines verified by commandParser method
            Hashtable variables = new Hashtable();              //Stores the variables
            ArrayList lines = new ArrayList();              //Stores the block of lines to be tester
            Method testMethodObject;
            lines.Add("method testMethod(param1, param2, param3)");
            lines.Add("drawto param1,param2");
            lines.Add("square param3");
            lines.Add("endmethod");
            lines.Add("testMethod(100,200,300)");
            lines.Add("testMethod(150,250,350)");

            // ACT
            int lineNo = 1;
            foreach (String line in lines)
            {   //calls the command parser method that analyzes the method call command
                cmdParser.parseCommand(pQueue, line, variables, lineNo++);
            }
            testMethodObject = (Method)cmdParser.StoredMethods["testmethod"];
            int expectedQueueSize = 4;
            int actualQueueSize = pQueue.size();
            //These are the expected lines that should be added to the program queu for execution 
            //after the method call
            String expectedAddedCommand1 = "drawto 100,200";
            String expectedAddedCommand2 = "square 300";
            String expectedAddedCommand3 = "drawto 150,250";
            String expectedAddedCommand4 = "square 350";
            String actualAddedCommand1 = pQueue.dequeue();
            String actualAddedCommand2 = pQueue.dequeue();
            String actualAddedCommand3 = pQueue.dequeue();
            String actualAddedCommand4 = pQueue.dequeue();

            // ASSERT         
            
            Assert.AreEqual(expectedAddedCommand1, actualAddedCommand1, "Statements were not added from the method block properly after being called.");
            Assert.AreEqual(expectedAddedCommand2, actualAddedCommand2, "Statements were not added from the method block properly after being called.");
            Assert.AreEqual(expectedAddedCommand3, actualAddedCommand3, "Statements were not added from the method block properly after being called.");
            Assert.AreEqual(expectedAddedCommand4, actualAddedCommand4, "Statements were not added from the method block properly after being called.");
            Assert.AreEqual(expectedQueueSize, actualQueueSize, 0.01, "Method did not add expected number of statements.");
        }
    }
}
