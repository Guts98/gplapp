﻿
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphicalProgrammingLanguageApp;
using System.Collections;

namespace GPLUnitTest
{
    /// <summary>
    /// Tests if the parseCommand method successfully reads the valid WHILE command and its condition, when its true, it should add the 
    /// block statements inside the WHILE LOOP, and after iterating through the block statements untill the condition is true,
    /// it should then add the final statements to the program queue
    /// </summary>
    [TestClass]
    public class WHILECommandTest
    {
        /// <summary>
        /// Tests if the parseCommand method successfully reads the valid while command and its condition, when its true, it should 
        /// iterate through the statements of the while block and store the draw commands in the program queue
        /// </summary>
        [TestMethod]
        public void Test_commandWHILETrueValid()
        {
            // ARRANGE
            CommandParser cmdParser = new CommandParser();      //Command Parser object to access its methods
            ProgramQueue pQueue = new ProgramQueue();           //ProgramQueue object to store the lines verified by commandParser method
            Hashtable variables = new Hashtable();              //Stores the variables
            ArrayList lines = new ArrayList();              //Stores the block of lines to be tester
            lines.Add("a=0");
            lines.Add("while a<3");
            lines.Add("circle a");
            lines.Add("a = a + 1");
            lines.Add("endwhile");

            // ACT
            int lineNo = 1;
            foreach (String line in lines)
            {
                cmdParser.parseCommand(pQueue, line, variables, lineNo++);  //calls the command parser method that analyzes the while command

            }

            // ASSERT                                          
            //circle 0, circle 1, and circle 2 lines will be added since while condition was true and iterates 3 times over circle a
           int expectedQueueSize = 3;
            int actualQueueSize = pQueue.size();
            String expectedAddedCommand1 = "circle 0";
            String expectedAddedCommand2 = "circle 1";
            String expectedAddedCommand3 = "circle 2";
            String actualAddedCommand1 = pQueue.dequeue();
            String actualAddedCommand2 = pQueue.dequeue();
            String actualAddedCommand3 = pQueue.dequeue();

            Assert.AreEqual(expectedAddedCommand1, actualAddedCommand1, "While command was not parsed or executed successfully by parseCommand method.");
            Assert.AreEqual(expectedAddedCommand2, actualAddedCommand2, "WHile command was not parsed or executed successfully by parseCommand method.");
            Assert.AreEqual(expectedAddedCommand3, actualAddedCommand3, "While command was not parsed or executed successfully by parseCommand method.");
            Assert.AreEqual(expectedQueueSize, actualQueueSize, 0.01, "While command was not parsed or executed successfully by parseCommand method.");

        }


        /// <summary>
        /// Tests if the parseCommand method successfully reads the valid while command and its condition, when its false, it should not add the 
        /// block statements inside the while to the program queue
        /// </summary>
        [TestMethod]
        public void Test_commandWHILEFalseValid()
        {
            // ARRANGE
            CommandParser cmdParser = new CommandParser();      //Command Parser object to access its methods
            ProgramQueue pQueue = new ProgramQueue();           //ProgramQueue object to store the lines verified by commandParser method
            Hashtable variables = new Hashtable();              //Stores the variables
            ArrayList lines = new ArrayList();              //Stores the block of lines to be tested
            lines.Add("a=0");
            lines.Add("while a>3");
            lines.Add("circle a");
            lines.Add("a = a + 1");
            lines.Add("endwhile");

            // ACT
            int lineNo = 1;
            foreach (String line in lines)
            {
                cmdParser.parseCommand(pQueue, line, variables, lineNo++);  //calls the command parser method that analyzes the while command
            }

            // ASSERT                   //NO Lines will be added since while condition was false
            int expectedQueueSize = 0;
            int actualQueueSize = pQueue.size();
            Assert.AreEqual(expectedQueueSize, actualQueueSize, 0.01, "If command was not parsed successfully by parseCommand method.");

        }

    }
}
