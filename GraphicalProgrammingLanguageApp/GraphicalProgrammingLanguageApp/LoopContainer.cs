﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Container interface that returns a new object of <see cref="Iterator"/> class
    /// </summary>
    interface IContainer
    {
        /// <summary>
        /// Returns a new object of the iterator 
        /// </summary>
        /// <returns></returns>
         Iterator getIterator();
    }
}
