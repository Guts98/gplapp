﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphicalProgrammingLanguageApp;
using System.Collections;

namespace GPLUnitTest
{
    /// <summary>
    /// Tests the parseCommand method by passing it different valid commands and checks whether it updates the test canvas's fill and pen's properties
    /// </summary>
    [TestClass]
    public class TestParser
    {
        /// <summary>
        /// Tests if the parseCommand method successfully reads the valid programs and updates the canvas object associated 
        /// </summary>
        [TestMethod]
        public void Test_parseMoveToValid()
        {
            // ARRANGE
            DrawCommands parser = new DrawCommands();               //Parser object to test its method
            Canvas testCanvas = new Canvas();           //canvas object whose methods are called by the parser to update canvas property 
            ArrayList shapeList = new ArrayList();      //ArrayList in which the parser adds the valid shapes
            Hashtable variables = new Hashtable();      //Dummy hashtable object for variables
            int expectedX = 100;                        //expected x position of canvas after testing with moveTo command
            int expectedY = 150;                        //expected y position of canvas after testing with moveTo command
            float expectedPenSize = 5;                  //expected pen size of canvas' pen after testing with pensize command

            // ACT
            parser.parseCommand("moveto 100,150", testCanvas, shapeList, variables);       //pass a valid command to parse command to change cursor position of canvas
            parser.parseCommand("pensize 5", testCanvas, shapeList, variables);            //pass a valid command to parse command to change the pen size of canvas

            // ASSERT
            int actualX = testCanvas.XPos;                                      //x position value set by the moveto command after being parsed
            int actualY = testCanvas.YPos;                                      //y position value set by the moveto command after being parsed
            float actualPenColor = testCanvas.pen.Width;                        //pen size value set by the pensize command after being parsed
            Assert.AreEqual(expectedX, actualX, 0.01, "ParseCommand method did not run properly.");                 //testing actual and expected parameters
            Assert.AreEqual(expectedY, actualY, 0.01, "ParseCommand method did not run properly.");
            Assert.AreEqual(expectedPenSize, actualPenColor, 0.01, "ParseCommand method did not run properly.");
        }

    }
}
