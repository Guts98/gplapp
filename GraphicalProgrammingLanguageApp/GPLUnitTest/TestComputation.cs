﻿using System;
using System.Collections;
using GraphicalProgrammingLanguageApp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GPLUnitTest
{
    [TestClass]
    public class TestComputation
    {
        /// <summary>
        /// Tests if the computation of an expression yields the right result
        /// </summary>
        [TestMethod]
        public void TestComputeExpression()
        {
            // ARRANGE
            CommandParser cmdParser = new CommandParser();      //Command Parser object to access its methods
            
            // ACT
            String expectedResult = "121";
            String actualResult = cmdParser.computeExpression("11*11");

            // ASSERT                   //The compute expression method should return the product of 11*11
            Assert.AreEqual(expectedResult, actualResult, "There was an error in compmuting expression.");

        }

        /// <summary>
        /// Tests if the computation of an conditional expression yields the right bool result
        /// </summary>
        [TestMethod]
        public void TestComputeCondition()
        {
            // ARRANGE
            CommandParser cmdParser = new CommandParser();      //Command Parser object to access its methods

            // ACT
            bool expectedResult = false;
            bool actualResult = cmdParser.computeCondition("255 < 100");

            // ASSERT                   //The compute conditional expression method should return false
            Assert.AreEqual(expectedResult, actualResult, "There was an error in compmuting condiitonal expression.");

        }
    }
}
