﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphicalProgrammingLanguageApp;
using System.Collections;

namespace GPLUnitTest

{
    /// <summary>
    /// Tests the implementation of variables in the program
    /// </summary>
    [TestClass]
    public class TestVariable
    {
        /// <summary>
        /// Tests if a variable's value is assigned properly
        /// </summary>
        [TestMethod]
        public void TestVariablesAssign()
        {
            // ARRANGE
            CommandParser cmdParser = new CommandParser();      //Command Parser object to access its methods
            ProgramQueue pQueue = new ProgramQueue();           //ProgramQueue object to store the lines verified by commandParser method
            Hashtable variables = new Hashtable();              //Stores the variables
            ArrayList lines = new ArrayList();              //Stores the block of lines to be tested
            lines.Add("a=100");

            // ACT
            int lineNo = 1;
            foreach (String line in lines)
            {
                cmdParser.parseCommand(pQueue, line, variables, lineNo++);  //calls the command parser method that analyzes the while command
            }

            // ASSERT                   //NO Lines will be added since while condition was false
            String expectedVarValue = "100";
            String actualVarValue = variables["a"].ToString();
            Assert.AreEqual(expectedVarValue, actualVarValue,  "Variable Value was not assigned properly.");

        }

        /// <summary>
        /// Tests if a variable is being reassigned successfully
        /// </summary>
        [TestMethod]
        public void TestVariableReassign()
        {
            // ARRANGE
            CommandParser cmdParser = new CommandParser();      //Command Parser object to access its methods
            ProgramQueue pQueue = new ProgramQueue();           //ProgramQueue object to store the lines verified by commandParser method
            Hashtable variables = new Hashtable();              //Stores the variables
            ArrayList lines = new ArrayList();              //Stores the block of lines to be tested
            lines.Add("a=100");
            lines.Add("a=a-70");

            // ACT
            int lineNo = 1;
            foreach (String line in lines)
            {
                cmdParser.parseCommand(pQueue, line, variables, lineNo++);  //calls the command parser method that analyzes the while command
            }

            // ASSERT                   //NO Lines will be added since while condition was false
            String expectedVarValue = "30";
            String actualVarValue = variables["a"].ToString();
            Assert.AreEqual(expectedVarValue, actualVarValue, "Variable Value was not re-assigned properly.");
        }

        /// <summary>
        /// Tests if this method replaces the variables rom hashtable with its value in a statement
        /// </summary>
        [TestMethod]
        public void TestPutVariableValues()
        {
            // ARRANGE
            CommandParser cmdParser = new CommandParser();      //Command Parser object to access its methods
            ProgramQueue pQueue = new ProgramQueue();           //ProgramQueue object to store the lines verified by commandParser method
            Hashtable variables = new Hashtable();              //Stores the variables
            variables.Add("a", "100");
            variables.Add("b", "200");
            String line = "drawto a,b";

            // ACT
            String expectedVarValue = "drawto 100,200";
            String actualVarValue = cmdParser.putVariableValues(line, variables);

            // ASSERT          
            Assert.AreEqual(expectedVarValue, actualVarValue, "Variable were not replaced with values in the statement.");

        }
    }
    
}
