﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// SntaxHelpForm class that defines a new form class to display syntax guide info by fetching it from the local database
    /// </summary>
    /// <remarks>Partially Inherits <see cref="System.Windows.Forms"/></remarks>
    public partial class SyntaxHelpForm : Form
    {
        /// <summary>
        /// Default Constructor that initializes the form 
        /// </summary>
        public SyntaxHelpForm()
        {
            InitializeComponent();              //initialize the form
            //Set the text to the labels on GUI
            supportedColorListLabel.Text = "RED, BLUE, GREEN, BLACK, WHITE, ORANGE, PINK, PURPLE, YELLOW";
            hexColorInfoLabel.Text = "(Hex Color Codes are also valid!)";
            fillValuesLabel.Text = "Only 'On' or 'Off' values are allowed.";
        }

        /// <summary>
        /// ActionListener for when the form loads
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>It loads the data from local database to the table adapter</remarks>
        private void Form2_Load(object sender, EventArgs e)
        {
            // This line of code loads data into the 'gplSyntaxDBDataSet.gplSyntaxes' table. 
            this.gplSyntaxesTableAdapter.Fill(this.gplSyntaxDBDataSet.gplSyntaxes);
            StyleDataGridView();

        }

        /// <summary>
        /// Sets the styling properties of the data grid view table
        /// </summary>
        public void StyleDataGridView()
        {
            gplSyntaxesDataGridView.BorderStyle = BorderStyle.None;
            gplSyntaxesDataGridView.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            gplSyntaxesDataGridView.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            gplSyntaxesDataGridView.DefaultCellStyle.SelectionBackColor = Color.SeaGreen;
            gplSyntaxesDataGridView.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            gplSyntaxesDataGridView.BackgroundColor = Color.FromArgb(30, 30, 30);
           // gplSyntaxesDataGridView.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;//optional
            gplSyntaxesDataGridView.EnableHeadersVisualStyles = false;
            gplSyntaxesDataGridView.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            // gplSyntaxesDataGridView.ColumnHeadersDefaultCellStyle.Font = new Font("Monsterrat", 10);
            gplSyntaxesDataGridView.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(62, 100, 255);
             gplSyntaxesDataGridView.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(236, 252, 255);
        }
    }
}
