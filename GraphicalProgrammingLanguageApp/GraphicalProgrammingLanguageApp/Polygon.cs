﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Triangle class used to define and draw polygon shape
    /// </summary>
    /// <remarks>Inherits <see cref="abstract class Shape : Shapes"/></remarks>
    class Polygon : Shape
    {
        /// <summary>
        /// Points of the polygon..x and y
        /// </summary>
        Point[] vertices;

        /// <summary>
        /// Default Constructor 
        /// </summary>
        /// <remarks>Calls base constructor to define color, x and y </remarks>
        public Polygon() : base()
        {
        }

        /// <summary>
        /// Parameterized constructor that provides the color,num of vertices and side width as well as cursor position of the polygon
        /// </summary>
        /// <param name="color">Outline color for the polygon</param>
        /// <param name="x">X position on the graphics</param>
        /// <param name="y">Y position on the graphics</param>
        /// <remarks>Calls base constructor to define color, x and y </remarks>
        public Polygon(Color color, int x, int y) : base(color, x, y)
        {
            
        }

        /// <summary>
        /// Sets the color, cursor position as well as the baseSide and height of the triangle    
        /// </summary>
        /// <param name="color">Outline color for the polygon</param>
        /// <param name="list">Integer parameter list containing x position, y positoin for the polygon</param>
        public override void set(Color color, params int[] list)
        {
            if (((list.Length) < 6) || ((list.Length) % 2 != 0))                  //check and reports invalid number of parameters for this shape
                throw new InvalidParameterException("Wrong number of parameters '" + (list.Length - 2) + "' given, min 2 expected for a polygon");

            base.set(color, list[0], list[1]);           //list[0] and list[1] being x and y coordinates 

            this.vertices = new Point[(list.Length - 2)/2];
            int j = 0;
            for(int i=2;i< list.Length ;i+=2)
            {
                Console.WriteLine("Point " + (j + 1) + ": " + list[i]+ list[i + 1]);
                vertices[j++] = new Point( list[i], list[i + 1] );
            }
        }

        /// <summary>
        /// Draws polygon on the provided graphics 
        /// </summary>
        /// <param name="g">The graphics provided by the caller on which the triangle is to be drawn</param>
        /// <remarks>The triangle is drawn by deducing 3 vertices from the height and width provided and then drawing a polygon out of those vertices.</remarks>
        public override void draw(Graphics g)
        {
            Pen p = new Pen(base.PenColor, base.PenSize);             //uses the color and pen size defined in shape by default
            SolidBrush sb = new SolidBrush(base.FillColor);              //new solid brush to fill shapes wth the color defined in shape class by default
 

            //Draw Triangle polygon Shape on the provided graphics
            if (FillStatus)                             //if fill is on
            {
                g.DrawPolygon(p, this.vertices);         //draw hollow triangle
                g.FillPolygon(sb, this.vertices);        //draw filled triangle
            }
            else
                g.DrawPolygon(p, this.vertices);         //draw hollow triangle         
        }

        /// <summary>
        /// Calculates the surface area of the polygon.
        /// </summary>
        /// <returns>Calculated area of the polygon.</returns>
        public override double calcArea()
        {
            return 0;// ((numVertex * side) / 2);
        }

        /// <summary>
        /// Calculates the perimeter of the polygon.
        /// </summary>
        /// <returns>Calculated Parameter of the polygon.</returns>
        public override double calcPerimeter()
        {
            //double s1, s2, s3 = this.numVertex;
            //s1 = s2 = (numVertex + System.Math.Sqrt(System.Math.Pow(this.numVertex / 2, 2) + System.Math.Pow(this.side / 2, 2)));

            return 0;// (s1 + s2 + s3);
        }
    }
}
