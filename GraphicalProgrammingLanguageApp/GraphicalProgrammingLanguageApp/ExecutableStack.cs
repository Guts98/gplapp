﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Executable stack which implements <see cref="Stack"/> interface, that takes one draw command line at a tome from <see cref="ProgramQueue"/> object 
    /// and helps in executing them in order
    /// </summary>
    class ExecutableStack : Stack
    {
        /// <summary>
        /// Stack in which individual draw commands will be loaded from program queue one at a time for execution
        /// </summary>
         Stack<String> executableLine;

        /// <summary>
        /// Default constructor for ExecutableStack class that initializes a new object to the executableLine
        /// </summary>
        public ExecutableStack()
        {
            executableLine = new Stack<String>();
        }

        /// <summary>
        /// Adds or pushes a line to top of the stack
        /// </summary>
        /// <param name="line">line to be pushed</param>
        public void push(String line)
        {
            executableLine.Push(line);
        }

        /// <summary>
        /// Removes or pops out and returns the line from top of the stack
        /// </summary>
        /// <returns>The line just popped out</returns>
        public String pop()
        {
            return executableLine.Pop();
        }

        /// <summary>
        /// Returns or peeks at the line at the top of the stack
        /// </summary>
        /// <returns>line at the top of the stack</returns>
        public String peek()
        {
           return executableLine.Peek();
        }

        /// <summary>
        /// Returns the count of lines stored in the stack
        /// </summary>
        /// <returns>count of the lines in the stack</returns>
        public int size()
        {
            return executableLine.Count();
        }

        /// <summary>
        /// Clears the stack clean
        /// </summary>
        public void clear()
        {
            executableLine.Clear();
        }
    }
}
