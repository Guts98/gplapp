﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Program Queue class, contains a queue data structure to hold the draw commands deducted by
    /// the graphical programming language in proper order
    /// </summary>
    public class ProgramQueue : Queue
    {
        /// <summary>
        /// Queue data structure to store the draw command lines for later execution
        /// </summary>
        Queue<String> programLines;

        /// <summary>
        /// Holds the line numbers of each draw commands sequentiall
        /// </summary>
        private ArrayList lineNumbers;

        /// <summary>
        /// Sets and Returns the lineNumbers
        /// </summary>
        public ArrayList LineNumbers { get => lineNumbers; set => lineNumbers = value; }

        /// <summary>
        /// Adds the given line number to the lineNumbers
        /// </summary>
        /// <param name="num">line number to be added</param>
        public void addLineNum(int num)
        {
            LineNumbers.Add(num);
        }

        /// <summary>
        /// Displays the lineNumbers arraylist (Not a functional method)
        /// </summary>
        public void displaylineNums()
        {
            foreach(int a in lineNumbers)
            {
                Console.Write(a + ", ");
            }
        }

        /// <summary>
        /// Returns the line number for the given index (which 
        /// matches the draw commands' index in statements arraylist)
        /// </summary>
        /// <param name="index">index value whose line number is required to be returned</param>
        /// <returns>line number at the given index</returns>
        public int getLineNum(int index)
        {
            return Convert.ToInt32(LineNumbers[index]);
        }

        /// <summary>
        /// Default constructor that initializes the queue and lineNumbers arraylist
        /// </summary>
        public ProgramQueue()
        {
            lineNumbers = new ArrayList();
            programLines = new Queue<String>();
        }
        
        /// <summary>
        /// Adds the draw command statement at the end of the programLines queue
        /// </summary>
        /// <param name="line">statement to be added to the queue</param>
        public void enqueue(String line)
        {
            programLines.Enqueue(line);
        }

        /// <summary>
        /// Removes the line from the top of the queue
        /// </summary>
        /// <returns>The line just removed</returns>
        public String dequeue()
        {
            return programLines.Dequeue();
        }

        /// <summary>
        /// Returns the line at the top of the queue
        /// </summary>
        /// <returns>The line at the top of the queue</returns>
        public String peek()
        {
            return programLines.Peek();
        }

        /// <summary>
        /// Returns the size of the queue
        /// </summary>
        /// <returns>The size of the queue</returns>
        public int size()
        {
             return programLines.Count();   
        }

        /// <summary>
        /// Clears the queue of all its lines
        /// </summary>
        public void clear()
        {
            programLines.Clear();
        }
           
    }
}
