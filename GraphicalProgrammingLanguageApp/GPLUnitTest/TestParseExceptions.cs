﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphicalProgrammingLanguageApp;
using System.Collections;

namespace GPLUnitTest
{
    /// <summary>
    /// Tests if all the exceptions are thrown properly while parsing a command 
    /// </summary>
    [TestClass]
    public class TestParseExceptions
    {
        /// <summary>
        /// Tests if ParameterFormatException is thrown by passing an invalid parameter to the parser
        /// </summary>
        [TestMethod]
       public void Test_ParameterFormatException()
        {
            // ARRANGE
            DrawCommands parser = new DrawCommands();
            Canvas testCanvas = new Canvas();
            ArrayList shapeList = new ArrayList();
            Hashtable variables = new Hashtable();      //Dummy hashtable object for variables
            String ParameterFormatException = "wrong parameter format '23s' given";


            // ACT
            try
            {
                parser.parseCommand("moveto 23s,23s", testCanvas, shapeList, variables);
            }
            catch (GraphicalProgrammingLanguageApp.InvalidParameterException e)
            {
                // ASSERT
                StringAssert.Contains(e.Message.ToLower(), ParameterFormatException);
                return;
            }

            Assert.Fail("ParameterFormatException was not thrown!");
        }

        /// <summary>
        /// Tests if InvalidColorException is thrown by passing an invalid color to the parser
        /// </summary>
        [TestMethod]
        public void Test_InvalidColorException()
        {
            // ARRANGE
            DrawCommands parser = new DrawCommands();
            Canvas testCanvas = new Canvas();
            ArrayList shapeList = new ArrayList();
            Hashtable variables = new Hashtable();      //Dummy hashtable object for variables
            String InvalidColorException = "wrongcolor";

            // ACT
            try
            {
                parser.parseCommand("pencolor wrongColor", testCanvas, shapeList, variables);
            }
            catch (GraphicalProgrammingLanguageApp.InvalidColorException e)
            {
                // ASSERT
                StringAssert.Contains(e.Message.ToLower(), InvalidColorException);
                return;
            }
            Assert.Fail("InvalidColorException was not thrown!");
        }

        /// <summary>
        /// Tests if InvalidCommandException is thrown by passing an invalid command name to the parser
        /// </summary>
        [TestMethod]
        public void Test_InvalidCommandException()
        {
            // ARRANGE
            DrawCommands parser = new DrawCommands();
            Canvas testCanvas = new Canvas();
            ArrayList shapeList = new ArrayList();
            Hashtable variables = new Hashtable();      //Dummy hashtable object for variables
            String InvalidCommandException = "invalid command 'wrongcommand' encountered";

            // ACT
            try
            {
                parser.parseCommand("wrongCommand 25,25", testCanvas, shapeList, variables);
            }
            catch (GraphicalProgrammingLanguageApp.InvalidCommandException e)
            {
                // ASSERT
                StringAssert.Contains(e.message.ToLower(), InvalidCommandException);
                return;
            }

            Assert.Fail("InvalidCommandException was not thrown!");
        }

        /// <summary>
        /// Tests if InvalidPenSizeException is thrown by passing an invalid pen size i.e. no in the range of 1 to 20 to the parser
        /// </summary>
        [TestMethod]
        public void Test_InvalidPenSizeException()
        {
            // ARRANGE
            DrawCommands parser = new DrawCommands();
            Canvas testCanvas = new Canvas();
            ArrayList shapeList = new ArrayList();
            Hashtable variables = new Hashtable();      //Dummy hashtable object for variables
            String InvalidPenSizeException = "1000";

            // ACT
            try
            {
                parser.parseCommand("pensize 1000", testCanvas, shapeList, variables);
            }
            catch (GraphicalProgrammingLanguageApp.InvalidPenSizeException e)
            {
                // ASSERT
                StringAssert.Contains(e.Message.ToLower(), InvalidPenSizeException);
                return;
            }
            Assert.Fail("InvalidPenSizeException was not thrown!");
        }

        /// <summary>
        /// Tests if InvalidSyntaxException is thrown by passing an invalid syntax i.e. wrong number of words to be a command to the parser
        /// </summary>
        [TestMethod]
        public void Test_InvalidSyntaxException()
        {
            // ARRANGE
            DrawCommands parser = new DrawCommands();
            Canvas testCanvas = new Canvas();
            ArrayList shapeList = new ArrayList();
            Hashtable variables = new Hashtable();      //Dummy hashtable object for variables
            String InvalidSyntaxException = "wrong number of tokens '3' supplied, 2 expected";

            // ACT
            try
            {
                parser.parseCommand("moveto 20,20 30,40", testCanvas, shapeList, variables);
            }
            catch (GraphicalProgrammingLanguageApp.InvalidSyntaxException e)
            {
                // ASSERT
                StringAssert.Contains(e.Message.ToLower(), InvalidSyntaxException);
                return;
            }
            Assert.Fail("InvalidSyntaxException was not thrown!");

        }

        /// <summary>
        /// Tests if InvalidParameterLengthException is thrown by passing an invalid length of parameters to the parser
        /// </summary>
        [TestMethod]
        public void Test_InvalidParameterLengthException()
        {
            // ARRANGE
            DrawCommands parser = new DrawCommands();
            Canvas testCanvas = new Canvas();
            ArrayList shapeList = new ArrayList();
            Hashtable variables = new Hashtable();      //Dummy hashtable object for variables
            String InvalidParameterLengthException = "wrong number of parameters '4' given, 1 expected for a square";

            // ACT
            try
            {
                parser.parseCommand("square 25,25,25,25", testCanvas, shapeList, variables);
            }
            catch (GraphicalProgrammingLanguageApp.InvalidParameterException e)
            {
                // ASSERT
                StringAssert.Contains(e.Message.ToLower(), InvalidParameterLengthException);
                return;
            }
            Assert.Fail("InvalidParameterException was not thrown!");
        }

        /// <summary>
        /// Tests if ComputationalException is thrown by passing an invalid expression to the computeExpression 
        /// method
        /// </summary>
        [TestMethod]
        public void Test_ComputationalException()
        {
            // ARRANGE
            // ARRANGE
            CommandParser cmdParser = new CommandParser();      //Command Parser object to access its methods
            String ComputationalException = "error in computing '255 +asd 1a00' condition";
           
            // ACT
            try
            {
                bool actualResult = cmdParser.computeCondition("255 +asd 1a00");
            }
            catch (GraphicalProgrammingLanguageApp.ComputationalException e)
            {
                // ASSERT
                StringAssert.Contains(e.Message.ToLower(), ComputationalException);
                return;
            }
            Assert.Fail("ComputationalException was not thrown!");
        }

        /// <summary>
        /// Tests if MethodDeclarationException is thrown by trying to define a method inside a if block
        /// </summary>
        [TestMethod]
        public void Test_MethodDeclarationException()
        {
            // ARRANGE
            CommandParser cmdParser = new CommandParser();      //Command Parser object to access its methods
            ProgramQueue pQueue = new ProgramQueue();           //ProgramQueue object to store the lines verified by commandParser method
            Hashtable variables = new Hashtable();              //Stores the variables
            ArrayList lines = new ArrayList();              //Stores the block of lines to be tester
            String MethodDeclarationException = "method declaration can't be inside a 'if' or 'while' block";
            lines.Add("if 50>20");
            lines.Add("method testMethod(param1, param2, param3)");
            lines.Add("drawto param1,param2");
            lines.Add("square param3");
            lines.Add("endmethod");
            lines.Add("endif");

            // ACT
            try
            {
                // ACT
                int lineNo = 1;
                foreach (String line in lines)
                {   //calls the command parser method that analyzes the method declaration command
                    cmdParser.parseCommand(pQueue, line, variables, lineNo++);
                }
            }
            catch (GraphicalProgrammingLanguageApp.MethodDeclarationException e)
            {
                // ASSERT
                StringAssert.Contains(e.Message.ToLower(), MethodDeclarationException);
                return;
            }
            Assert.Fail("MethodDeclarationException was not thrown!");
        }

        /// <summary>
        ///  Tests if MethodDeclarationException is thrown by trying to call a method by passing undefined
        ///  arguments inside the call brackets
        /// </summary>
        [TestMethod]
        public void Test_MethodCallException()
        {
            // ARRANGE
            CommandParser cmdParser = new CommandParser();      //Command Parser object to access its methods
            ProgramQueue pQueue = new ProgramQueue();           //ProgramQueue object to store the lines verified by commandParser method
            Hashtable variables = new Hashtable();              //Stores the variables
            ArrayList lines = new ArrayList();              //Stores the block of lines to be tester
            String MethodCallException = "invalid parameters passed ";
            lines.Add("method testMethod(param1, param2, param3)");
            lines.Add("drawto param1,param2");
            lines.Add("square param3");
            lines.Add("endmethod");
            lines.Add("testMethod(undefinedVar1, undefinedVar2, undefinedVar3)");

            // ACT
            try
            {
                // ACT
                int lineNo = 1;
                foreach (String line in lines)
                {   //calls the command parser method that analyzes the method call command
                    cmdParser.parseCommand(pQueue, line, variables, lineNo++);
                }
            }
            catch (GraphicalProgrammingLanguageApp.MethodCallException e)
            {
                // ASSERT
                StringAssert.Contains(e.Message.ToLower(), MethodCallException);
                return;
            }
            Assert.Fail("MethodCallException was not thrown!");
        }

    }
}
