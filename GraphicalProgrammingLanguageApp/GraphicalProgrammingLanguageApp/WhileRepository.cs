﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// While Repository class that implements <see cref="IContainer"/>interface and has a method 
    /// that returns an iterator object when invoked.
    /// </summary>
    class WhileRepository:IContainer
    {
        /// <summary>
        /// Returns a new object of WhileRepository class
        /// </summary>
        /// <returns>New While Iterator Object</returns>
        public Iterator getIterator()
        {
            return new WhileIterator();
        }
       
        /// <summary>
        /// WhileIterator class that implements <see cref="Iterator"/> Interface and basically holds
        /// statements of the while block and has iterator functions like hasNext() and next() used
        /// while iterating the block of this while object
        /// </summary>
        private class WhileIterator : Iterator
        {
            /// <summary>
            /// Statements of the while block stored sequentially
            /// </summary>
            private ArrayList statements;

            /// <summary>
            /// Index of the statements arraylist while iterating through the loop
            /// </summary>
            private int index = 0;

            /// <summary>
            /// The While Condition based on which the next loop existence is decided
            /// </summary>
            String whileCondition = "";

            /// <summary>
            /// Bool value that tells if the while object is the outermost in a nested loop
            /// </summary>
            private bool isOutermostLoop;

            /// <summary>
            /// The bool value for the result of condition at the starting while command
            /// </summary>
            private bool startingConditionResult;

            /// <summary>
            /// Line number of the while command
            /// </summary>
            private int startingLineNum;

            /// <summary>
            /// Set true if the loop is the outermost loop of the iteration
            /// </summary>
            /// <param name="result">bool value for continuation of iteration</param>
            public void setIsOutermostLoop(bool result)
            {
                this.isOutermostLoop = result;
            }

            /// <summary>
            /// Returns true if the loop is the outermost loop of the iteration
            /// </summary>
            /// <returns>bool value for continuation of iteration</returns>
            public bool getIsOutermostLoop()
            {
                return this.isOutermostLoop;
            }


            /// <summary>
            /// Stores the initial conditional result for the first iteration of the loop
            /// </summary>
            /// <param name="result">initial conditional result</param>
            public void setStartingConditionResult(bool result)
            {
                this.startingConditionResult = result;
            }

            /// <summary>
            ///  Returns the initial conditional result for the first iteration of the loop
            /// </summary>
            /// <returns>initial conditional result</returns>
            public bool getStartingConditionResult()
            {
                return this.startingConditionResult;
            }

            /// <summary>
            /// Sets the starting line number of the loop block's starting line
            /// </summary>
            /// <param name="num">line number of the loop block's starting line</param>
            public void setStartingLineNum(int num)
            {
                this.startingLineNum = num;
            }

            /// <summary>
            /// Returns the starting line number of the loop block's starting line
            /// </summary>
            /// <returns>line number of the loop block's starting line</returns>
            public int getStartingLineNum()
            {
                return this.startingLineNum;
            }



            /// <summary>
            /// Custom toString method that returns the contents of the object as string
            /// </summary>
            /// <returns>String statement of whie condition and statement count</returns>
            public override String ToString()
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("WhileCondition: "+this.whileCondition+" No.of statements: "+statements.Count+" Statements:\n");
                sb.Append("Is Outermost Loop: "+this.getIsOutermostLoop()+" Starting condition result: "+this.getStartingConditionResult()+
                    " Starting Line Num: "+this.getStartingLineNum()+"\n");
                foreach (String st in statements)
                {
                    sb.Append(st + "\n");
                }
                return sb.ToString();
            }

            /// <summary>
            /// Default Constructor that initializes the arraylist to store the statements
            /// </summary>
            public WhileIterator()
            {
                statements = new ArrayList();
            }

            /// <summary>
            /// Sets the while condition for this object
            /// </summary>
            /// <param name="condition">While Condition for iteration</param>
            public void setWhileCondition(String condition)
            {
                this.whileCondition = condition;
            }

            /// <summary>
            /// Returns the while condition of this object
            /// </summary>
            /// <returns>While Condition</returns>
            public String getWhileCondition()
            {
                //if all the statements of the while block has been executed, reset the index back to zero
                //for next iteration 
                if (this.index >= this.statements.Count)    
                    this.index = 0;
                return this.whileCondition;
            }

            /// <summary>
            /// Adds a new statement to the while block for this object
            /// </summary>
            /// <param name="statement">The statement to be added</param>
            public void addStatement(String statement)
            {
                this.statements.Add(statement);
            }

            /// <summary>
            /// Sets the index value
            /// </summary>
            /// <param name="i">index value</param>
            public void setIndex(int i)
            {
                this.index = i;
            }

            /// <summary>
            /// Returns the index value
            /// </summary>
            /// <returns>index value</returns>
            public int getIndex()
            {
                return this.index ;
            }

            /// <summary>
            /// Returns bool value depending whether the object's block has next statement 
            /// </summary>
            /// <returns></returns>
            public bool hasNext()
            {
                if (this.index < this.statements.Count)
                {
                    return true;
                }
                return false;
            }

            /// <summary>
            /// Returns the next statement during the iteration 
            /// </summary>
            /// <returns>statement</returns>
            public Object next()
            {
                Console.WriteLine("Loop Statement:"+index+". "+this.statements[index]);
                if (this.hasNext())
                {
                    return this.statements[index++];
                }
                return null;
            }
        }
    }

    

}
