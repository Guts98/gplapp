clear 
reset
center
fill on
fillcolor #949cdf
rect 1030,460
fill off

z=10
method round(x,y,a)
z=a
moveto x,y 
while z<100 
star z 
z=z+10 
#circle z
endwhile 
endmethod  

pencolor #892cdc
round(150,400,10) 

pencolor black
round(150,300,10) 

pencolor red
round(150,200,10) 

pencolor black
round(150,100,10) 

pencolor blue
round(250,50,10)

pencolor black
round(350,100,10)

pencolor orange
round(450,150,10)

pencolor black
round(550,150,10)

pencolor green
round(650,100,10)

pencolor black
round(750,50,10)

pencolor #f05454
round(850,100,10)

pencolor black
round(850,200,10)

pencolor #f9813a
round(850,300,10)

pencolor black
round(850,400,10)


l=0
pensize 10
pencolor red
moveTo 0,250
while l<1030
drawto l,250
l=l+20
endwhile

pencolor #ffd66b
moveTo 1000,280
while l>=0
drawto l,280
l=l-20
endwhile

l=0
pensize 10
pencolor #222831
moveTo 0,3100
while l<1030
drawto l,310
l=l+20
endwhile


moveto 500,405
fill on
pencolor #f9813a
fillcolor #000000
star 150



