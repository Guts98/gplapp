﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicalProgrammingLanguageApp
{
    /// <summary>
    /// Square class used to define and draw square shape
    /// </summary>
    /// <remarks>Inherits <see cref="abstract class Shape : Shapes"/></remarks>
    class Square : Shape
    {
        /// <summary>
        /// Width of the square
        /// </summary>
        int width;

        /// <summary>
        /// Default Constructor that defines a default square with size 100 by 100
        /// </summary>
        /// <remarks>Calls base constructor to define color, x and y </remarks>
        public Square() : base()
        {
            this.width = 100;
        }

        /// <summary>
        /// Parameterized constructor that provides the color, width t as well as cursor position of the square
        /// </summary>
        /// <param name="color">Outline color for the square</param>
        /// <param name="x">X position on the graphics</param>
        /// <param name="y">Y position on the graphics</param>
        /// <param name="width">Width of the square</param>
        /// <remarks>Calls base constructor to define color, x and y </remarks>
        public Square(Color color, int x, int y, int width) : base(color, x, y)
        {
            this.width = width;                                         //set the width of the rectangle
        }

        /// <summary>
        /// Sets the color, cursor position as well as the width of the square    
        /// </summary>
        /// <param name="color">Outline color for the square</param>
        /// <param name="list">Integer parameter list containing x position, y positoin and width of the square</param>
        public override void set(Color color, params int[] list)
        {
            if ((list.Length) != 3)                                     //check and reports invalid number of parameters for this shape
                throw new InvalidParameterException("Wrong number of parameters '" + (list.Length - 2) + "' given, 1 expected for a square");

            base.set(color, list[0], list[1]);                          //list[0] and list[1] being x and y coordinates 
            this.width = list[2];
        }

        /// <summary>
        /// Draws square on the provided graphics 
        /// </summary>
        /// <param name="g">The graphics provided by the caller on which the square is to be drawn</param>
        public override void draw(Graphics g)
        {
            Pen p = new Pen(base.PenColor, base.PenSize);             //uses the color and pen size defined in shape by default
            SolidBrush sb = new SolidBrush(base.FillColor);              //new solid brush to fill shapes wth the color defined in shape class by default

            //Draw Rectangle Shape on the provided graphics
            if (FillStatus)                                                  //if fill is on
            {
                g.FillRectangle(sb, (base.x - width / 2), (base.y - width / 2), width, width);
                g.DrawRectangle(p, (base.x - width / 2), (base.y - width / 2), width, width);
            }
            else
                g.DrawRectangle(p, (base.x - width / 2), (base.y - width / 2), width, width);
        }

        /// <summary>
        /// Calculates the surface area of the square.
        /// </summary>
        /// <returns>Calculated area of the square.</returns>
        public override double calcArea()
        {
            return (width * width);
        }

        /// <summary>
        /// Calculates the perimeter of the square.
        /// </summary>
        /// <returns>Calculated Parameter of the square.</returns>
        public override double calcPerimeter()
        {
            return (4 * width );
        }
    }
}
